<?php
	$pageTitle = "Fractional C-Suite | Digital Marketing Case Study: Revtech Performance";
	$pageKeywords = "digital marketing case study revtech performance";
	$pageDesc = "A look at the digital marketing work Fractional-Csuite Performed for client: Revtech Performance.";


	require_once("../tehPHP/kulaHeader.php");
?>
<script src="../js/jquery.ez-bg-resize.js" type="text/javascript" charset="utf-8"></script>
<script>
	$(document).ready(function() {
		
	});
</script>
<style>
	.revTechCaseStudyBG{background-image: url(../layout/backgrounds/innaDentistCaseStudyBG.jpg);}
	.cSuiteBGDiv{display: -webkit-flex;display: -ms-flexbox;display: flex;-webkit-flex-direction: column;-ms-flex-direction: column;flex-direction: column;-webkit-justify-content: middle;-ms-flex-pack: middle;justify-content: middle;-webkit-align-items: center;-ms-flex-align: center;align-items: center;width: 100%;min-height: 700px;vertical-align: middle;padding-top: 5%;padding-bottom: 5%;background-size: cover;background-repeat: no-repeat;background-position: 75% 75%;position: relative;}
	.cSuiteBGDiv:before, .cSuiteBGDiv:after{box-sizing: border-box;}
	.cSuteCaseStudyLogoShell{text-align: center;}
	.cSuteCaseStudyLogo{max-width: 350px;}
	.cSuteCaseStudyP{max-width: 800px; color: #FFFFFF; font-size: 1.3em; margin: 40px 0 20px 0; text-align: center;}
	.cSuiteCaseStudyColumnShell{margin: 10px 20px;border: 3px solid white; display: block;}
	.cSuiteCaseStudyColumnTitle{text-transform: uppercase; font-weight: bold; text-align: center;  color: #FFFFFF; }
	.cSuiteCaseStudyTableWrapper{min-width: 300px; max-width: 900px;}
	.cSuiteCaseStudyColumnSubTitle{font-size: 1.1em; color: #FFFFFF; text-align: center;}
	.cSulteCaseStudyList{list-style: none; padding: 0; margin: 0;}
	.cSuiteCaseStudyMacBook{ max-width: 1000px; margin-top: -95px; position: relative; z-index: 5;}
	.cSuiteTextAlignCenter{text-align: center;}
	.cSuiteCaseStudyParagraphDescription{font-size: 1.4em;}
	.cSuiteImage100{width: 100%;}
	.cSuiteRevTechNoResults{margin-top: 180px; border-top: 1px solid #CDCDCD; border-left: 1px solid #CDCDCD;}
	.cSuiteGradientBG{padding: 60px 20px; min-height: 400px; width: 100%; background-image: linear-gradient(to right bottom, #18a3c4, #1f8faf, #237b9a, #236884, #22566f);}
	.cSuiteCaseStudyResultsParagraph{font-size: 1.1em; color: #FFFFFF;}

	@media screen and (max-width: 767px) {
		.cSuiteRevTechNoResults{margin-top: 0;}
		.cSuiteResultsBuffer{margin-top: 150px;}
			}
</style>
<div class="cSuiteBGDiv revTechCaseStudyBG">
	<div class="cSuteCaseStudyLogoShell">
		<img class="cSuteCaseStudyLogo" alt="Inna Dentist - No Logo" src="<?php echo $tehAbsoluteURL; ?>/layout/logos/revTechLogozz.png" />
	</div>
	<p class="cSuteCaseStudyP">
		http://www.dentistnyc2.com/
	</p>
	<div class="cSuiteWrapper cSuiteCaseStudyListShell">
		<div class="row">
			<div class="col-sm-12 col-md-4 col-lg-4">
				<ul class="cSuiteLeadershipServicesList">
					<div class="cSuiteCaseStudyColumnTitle">
						Industry
					</div>
					<div class="cSuiteCaseStudyColumnSubTitle">
						Dentistry
					</div>
				</ul>
			</div>
			<div class="col-sm-12 col-md-4 col-lg-4">
				<ul class="cSuiteLeadershipServicesList">
					<div class="cSuiteCaseStudyColumnTitle">
						Strategy
					</div>
					<div class="cSuiteCaseStudyColumnSubTitle">
						<ul class="cSulteCaseStudyList">
							<li>Pay Per Click</li>
							<li>Organic Keyword Targetting</li>
							<li>Content Writing/Coaching</li>
							<li>Social Media</li>
						</ul>
					</div>
				</ul>
			</div>
			<div class="col-sm-12 col-md-4 col-lg-4">
				<ul class="cSuiteLeadershipServicesList">
					<div class="cSuiteCaseStudyColumnTitle">
						Location
					</div>
					<div class="cSuiteCaseStudyColumnSubTitle">
						Midtown New York, NY
					</div>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="cSuiteWrapper cSuiteTextAlignCenter">
	<img  class="cSuiteCaseStudyMacBook" alt="RevTech Design" src="<?php echo $tehAbsoluteURL; ?>/layout/images/innaDentistCaseStudyMacBook.png"/>
</div>
<div class="cSuiteWrapper">
	<div class="row">
		<div class="col-sm-12 col-md-6 col-lg-6">
			<div class="cSuiteLetterTitleShell" style="font-size: .8em; margin-bottom: 0;">
				<div class="cSuiteLetterTitleLetter">
					S
				</div>
				<div class="cSuiteLetterTitle">
					<div class="cSuiteLetterTitleTop">
						The
					</div>
					<div class="cSuiteLetterTitleBottom">
						Situation Now
					</div>
				</div>
				<br /><br /><br /><br /><br /><br />
				<h4>General Site Observations</h4>
				<ul class="cSuiteCaseStudyParagraphDescription">
					<li>
						Excellent Blog Content.
					</li>
					<li>
						Google Analytics is installed and working.
					</li>
					<li>
						Contact Us Page contains all the info. Map, contact form, email, phone number.
					</li>
					<li>
						No Logo - Branding identification.
					</li>
					<li>
						Landing page is missing call to action buttons. 
					</li>
					<li>
						Should have domain email: imma@detistynyc2.com
					</li>
				</ul>
				<p class="cSuiteCaseStudyParagraphDescription">
					Great starter organic content. It looks like you've covered your bases, listing out the area of practices and providing very generalized overview of each service.  What's missing is the details.  No photos? No lengthy explanations of teeth whitening services for example. On the dental implants page, there is some good detail about what is an implant and what materials are used in creating them. What's missing is things like what the procedure entails, does it require a scan? Then lab work? How long does all this take? And a ball park for cost. Does insurance cover this? Are there ways I can make it easier for insurance to cover this procedure?  We're also missing some before/after photos or just some photos of some implants.  Photos explaining where they're attached.
				</p>
			</div>
		</div>
		<div class="col-sm-12 col-md-6 col-lg-6">
			<div>
				<br /><br /><br /><br /><br /><br /><br /><br />
				<ul style="list-style-type: none;" class="cSuiteCaseStudyParagraphDescription">
					<li>
						Google Analytics  <b>Yes</b>
					</li>
					<li>
						Google Business Listing <b>No</b>
					</li>
					<li>
						Yelp <b>Yes</b>
					</li>
					<li>
						Yahoo <b>Yes</b>
					</li>
					<li>
						Google Adwords Campaign <b>No</b>
					</li>
					<li>
						Domain search yields no 1 result? <b>yes</b>
					</li>
					<li>
						Facebook: https://www.facebook.com/innacherndds/
					</li>
					<li>
						Linked IN: https://www.linkedin.com/in/inna-chern-5787413b/
					</li>
					<li>
						Instagram: https://www.instagram.com/innacherndds/
					</li>
					<li>
						Google reports <b>8 backlinks</b>
					</li>
					<li>
						Google reports <b>35 pages for the domain</b>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="cSuiteWrapper">
	<div class="row">
		<div class="col-sm-12 col-md-6 col-lg-6">
			<div class="">
				<br /><br /><br /><br /><br /><br />
				<h4>Home Page Rework/Redesign</h4>
				<p class="cSuiteCaseStudyParagraphDescription">
					The home page / template page need to be completely redesigned.  They aren't 100% mobile friendly and it's lacking luster that bestows a professional image witp any visitors. It's missing call to action buttons on the front page directing folks to call or fill out the form to make an appointment.  The content pages need to be reworked to be more Google/SEO friendly. Clear titles w/ keywords, with calls to action on them as well.  Contact page has all the components, just needs a modern touch.
				</p>
				<h4>Organic Keyword Targetting</h4>
				<ul class="cSuiteCaseStudyParagraphDescription">
					<li>Expand and localize on the current content.</li>
				</ul>
				<p class="cSuiteCaseStudyParagraphDescription">
					<b>Localization:</b> We will target keywords like Midtown Invisalign - Midtown pediatric dentist, midtown pediatric dentist services.
				</p>
				<p class="cSuiteCaseStudyParagraphDescription">
					<b>Expand the Content:</b> Current content is lacking some key details of the services being offered and also needs to expand how the procedures take place and how long they take. Need professional photos or buy stock photos.
				</p>
			</div>
		</div>
		<div class="col-sm-12 col-md-6 col-lg-6">
			<div class="cSuiteLetterTitleShell" style="font-size: .8em; margin-bottom: 0;">
				<div class="cSuiteLetterTitleLetter">
					S
				</div>
				<div class="cSuiteLetterTitle">
					<div class="cSuiteLetterTitleTop">
						The
					</div>
					<div class="cSuiteLetterTitleBottom">
						Strategy
					</div>
				</div>
				<br /><br /><br /><br /><br /><br />
				<h4>Pay Per Click Budget: $250 Recommended</h4>
				<p class="cSuiteCaseStudyParagraphDescription">
					Strategy should be to atttract local audiences. Small radius < 1 mile.
				</p>
				<h4>Facebook Ads Budget: $250 Recommended</h4>
				<p class="cSuiteCaseStudyParagraphDescription">
					I think the best areas to focus for Facebook Ads would be:
				</p>
				<ul class="cSuiteCaseStudyParagraphDescription">
					<li>Invisalign</li>
					<li>Implants</li>
					<li>Teeth Whitening</li>
					<li>Sedation Dentistry</li>
				</ul>
				<p class="cSuiteCaseStudyParagraphDescription">
					Reason being, it should be easy to target the appropriate demographics. And these are the services that people might want to see pop up in Facebook and they click.	
				</p>
			</div>
		</div>
	</div>
</div>
<!--
<div class="cSuiteGradientBG">
	<div class="cSuiteWrapper">
		<div class="row">
			<div class="col-sm-12 col-md-5 col-lg-5">
				<div class="cSuiteLetterTitleLetter" style="font-color: #FFFFFF; margin-bottom: 0;">
					R
				</div>
				<div class="cSuiteLetterTitle">
					<div class="cSuiteLetterTitleTop">
						The
					</div>
					<div class="cSuiteLetterTitleBottom">
						Results
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-7 col-lg-7">
				<div class="row cSuiteResultsBuffer" style="font-size: 1.2em;">
					<div class="col-sm-4 col-md-4 col-lg-4">
						<ul class="cSuiteLeadershipServicesList">
							<div class="cSuiteCaseStudyColumnTitle">
								Traffic Growth
							</div>
							<div class="cSuiteCaseStudyColumnSubTitle">
								823%
							</div>
						</ul>
					</div>
					<div class="col-sm-4 col-md-4 col-lg-4">
						<ul class="cSuiteLeadershipServicesList">
							<div class="cSuiteCaseStudyColumnTitle">
								Lead Generation
							</div>
							<div class="cSuiteCaseStudyColumnSubTitle">
								4-12%
							</div>
						</ul>
					</div>
					<div class="col-sm-4 col-md-4 col-lg-4">
						<ul class="cSuiteLeadershipServicesList">
							<div class="cSuiteCaseStudyColumnTitle">
								ROI
							</div>
							<div class="cSuiteCaseStudyColumnSubTitle">
								3-600%
							</div>
						</ul>
					</div>
				</div>
				<p class="cSuiteCaseStudyResultsParagraph">
					Fractional's digital marketing leadership and strategy boosted Revtech to the top of the search engines both organically and with paid advertising. With an average ROI of about 450% lead generation cost is down to a minimal thanks to the Organic Keyword Targetting.
				</p>
				<p class="cSuiteCaseStudyResultsParagraph">
					Traffic numbers continue to increase month to month while revenues continue to climb.
				</p>
			</div>
		</div>
	</div>
</div>
-->

<div class="container cSuiteHeaderPadding">
	<h1 class="cSuiteMainTitle" style="font-weight: normal;">
		Let's Discuss Your <br /><b>Digital Marketing Leadership</b><br />Needs
	</h1>
	<div class="cSuiteGrowButtonShell">
		<a href="./contactCSuite.php" class="kulaLargerTitle cSuiteGrowButton" style="font-weight: normal; text-align: center;">
			Start Here
		</a>
	</div>
	<style>

	</style>
</div>
<?php
	require_once("../tehPHP/kulaFooter.php")
?>