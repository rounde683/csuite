<?php

	$pageTitle = "Fractional C-Suite | Digital Marketing Case Study: DMV Pool Service";
	$pageKeywords = "digital marketing case study virginial pool service: DMV Pool Service";
	$pageDesc = "A look at the digital marketing work Fractional-Csuite Performed for client: DMV Pool Service.";
	require_once("../tehPHP/kulaHeader.php");
?>

<script src="../js/jquery.ez-bg-resize.js" type="text/javascript" charset="utf-8"></script>

<script>

	$(document).ready(function() {

		

	});

</script>

<style>

	.revTechCaseStudyBG{background-image: url(../layout/digitalMarketing/caseStudies/dmvpoolservice/DMVPoolCaseStudyBG.jpg);}
	.cSuiteBGDiv{display: -webkit-flex;display: -ms-flexbox;display: flex;-webkit-flex-direction: column;-ms-flex-direction: column;flex-direction: column;-webkit-justify-content: middle;-ms-flex-pack: middle;justify-content: middle;-webkit-align-items: center;-ms-flex-align: center;align-items: center;width: 100%;min-height: 700px;vertical-align: middle;padding-top: 5%;padding-bottom: 5%;background-size: cover;background-repeat: no-repeat;background-position: 75% 75%;position: relative;}
	.cSuiteBGDiv:before, .cSuiteBGDiv:after{box-sizing: border-box;}
	.cSuteCaseStudyLogoShell{text-align: center;}
	.cSuteCaseStudyLogo{max-width: 350px;}
	.cSuteCaseStudyP{max-width: 800px; color: #FFFFFF; font-size: 1.3em; margin: 40px 0 20px 0; text-align: center;}
	.cSuiteCaseStudyColumnShell{margin: 10px 20px;border: 3px solid white; display: block;}
	.cSuiteCaseStudyColumnTitle{text-transform: uppercase; font-weight: bold; text-align: center;  color: #FFFFFF; }
	.cSuiteCaseStudyTableWrapper{min-width: 300px; max-width: 900px;}
	.cSuiteCaseStudyColumnSubTitle{font-size: 1.1em; color: #FFFFFF; text-align: center;}
	.cSulteCaseStudyList{list-style: none; padding: 0; margin: 0;}
	.cSuiteCaseStudyMacBook{ max-width: 1000px; margin-top: -95px; position: relative; z-index: 5;}
	.cSuiteTextAlignCenter{text-align: center;}
	.cSuiteCaseStudyParagraphDescription{font-size: 1.4em;}
	.cSuiteImage100{width: 100%;}
	.cSuiteRevTechNoResults{margin-top: 180px; border-top: 1px solid #CDCDCD; border-left: 1px solid #CDCDCD;}
	.cSuiteGradientBG{padding: 60px 20px; min-height: 400px; width: 100%; background-image: linear-gradient(to right bottom, #18a3c4, #1f8faf, #237b9a, #236884, #22566f);}
	.cSuiteCaseStudyResultsParagraph{font-size: 1.1em; color: #FFFFFF;}

	@media screen and (max-width: 767px)
	{
		.cSuiteRevTechNoResults{margin-top: 0;}
		.cSuiteResultsBuffer{margin-top: 150px;}
	}

</style>

<div class="cSuiteBGDiv revTechCaseStudyBG">
	<div class="cSuteCaseStudyLogoShell">
		<img class="cSuteCaseStudyLogo" alt="DMV Pool Service - No Logo" src="<?php echo $tehAbsoluteURL; ?>layout/digitalMarketing/caseStudies/dmvpoolservice/dmvLogo.png" />
	</div>
	<p class="cSuteCaseStudyP">
		http://www.dmvpoolservice.com/
	</p>
	<div class="cSuiteWrapper cSuiteCaseStudyListShell">
		<div class="row">
			<div class="col-sm-12 col-md-4 col-lg-4">
				<ul class="cSuiteLeadershipServicesList">
					<div class="cSuiteCaseStudyColumnTitle">
						Industry
					</div>
					<div class="cSuiteCaseStudyColumnSubTitle">
						Pool Services
					</div>
				</ul>
			</div>
			<div class="col-sm-12 col-md-4 col-lg-4">
				<ul class="cSuiteLeadershipServicesList">
					<div class="cSuiteCaseStudyColumnTitle">
						Strategy
					</div>
					<div class="cSuiteCaseStudyColumnSubTitle">
						<ul class="cSulteCaseStudyList">
							<li>Pay Per Click</li>
							<li>Organic Keyword Targetting</li>
							<li>Social Media</li>
						</ul>
					</div>
				</ul>
			</div>
			<div class="col-sm-12 col-md-4 col-lg-4">
				<ul class="cSuiteLeadershipServicesList">
					<div class="cSuiteCaseStudyColumnTitle">
						Location
					</div>
					<div class="cSuiteCaseStudyColumnSubTitle">
						Annandale, VA
					</div>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="cSuiteWrapper cSuiteTextAlignCenter">
	<img  class="cSuiteCaseStudyMacBook" alt="DMV Pool Service" src="<?php echo $tehAbsoluteURL; ?>layout/digitalMarketing/caseStudies/dmvpoolservice/dmvPoolCaseStudyMacBook.png"/>
</div>
<div class="cSuiteWrapper">
	<div class="row">
		<div class="col-sm-12 col-md-6 col-lg-6">
			<div class="cSuiteLetterTitleShell" style="font-size: .8em; margin-bottom: 0;">
				<div class="cSuiteLetterTitleLetter">
					S
				</div>
				<div class="cSuiteLetterTitle">
					<div class="cSuiteLetterTitleTop">
						The
					</div>
					<div class="cSuiteLetterTitleBottom">
						Situation Now
					</div>
				</div>
				<br /><br /><br /><br /><br /><br />
				<h4>General Site Observations</h4>
				<ul class="cSuiteCaseStudyParagraphDescription">
					<li>
						Excellent Written Content.
					</li>
					<li>
						Google Analytics is installed and working.
					</li>
					<li>
						Contact Us Page contains all the info. Map, contact form, email, phone number.
					</li>
					<li>
						Has Logo - Branding identification.
					</li>
					<li>
						Landing page is missing call to action buttons. 
					</li>
					<li>
						Should have domain email: dima@dmvpoolservice.com
					</li>
				</ul>
				<p class="cSuiteCaseStudyParagraphDescription">
					Great starter organic content. It looks like you've covered your bases, listing out the area of practices and providing very generalized overview of each service.  What's missing is the details.  No photos? No lengthy explanations of pool services. For example, certain content pages should broken down and separated like monthly pool cleaning services vs weekly pool cleaning services.  While the page "Opening Pool service" is an industry term it is not as well known as terms like "pool maintenance" or "weekly pool maintenance".
				</p>
			</div>
		</div>
		<div class="col-sm-12 col-md-6 col-lg-6">
			<div>
				<br /><br /><br /><br /><br /><br /><br /><br />
				<ul style="list-style-type: none;" class="cSuiteCaseStudyParagraphDescription">
					<li>
						Google Analytics  <b>Yes</b>
					</li>
					<li>
						Google Business Listing <b>Yes</b>
					</li>
					<li>
						Yelp <b>Yes</b>
					</li>
					<li>
						Yahoo <b>Yes</b>
					</li>
					<li>
						Google Adwords Campaign <b>No</b>
					</li>
					<li>
						Domain search yields no 1 result? <b>yes</b>
					</li>
					<li>
						Facebook: 
					</li>
					<li>
						Linked IN: 
					</li>
					<li>
						Instagram: 
					</li>
					<li>
						Pinterest: 
					</li>
					<li>
						Google reports <b>465 backlinks</b>
					</li>
					<li>
						Google reports <b>176 pages for the domain</b>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="cSuiteWrapper">
	<div class="row">
		<div class="col-sm-12 col-md-6 col-lg-6">
			<div class="">
				<br /><br /><br /><br /><br /><br />
				<h4>Home Page Rework/Redesign</h4>
				<p class="cSuiteCaseStudyParagraphDescription">
					The home page / template page need to be completely redesigned.  They aren't 100% mobile friendly and it's lacking luster that bestows a professional image witp any visitors. It's missing call to action buttons on the front page directing folks to call or fill out the form to make an appointment.  The content pages need to be reworked to be more Google/SEO friendly. Clear titles w/ keywords, with calls to action on them as well.  Contact page has all the components, just needs a modern touch.
				</p>
				<h4>Organic Keyword Targetting</h4>
				<ul class="cSuiteCaseStudyParagraphDescription">
					<li>Expand and localize on the current content.</li>
				</ul>
				<p class="cSuiteCaseStudyParagraphDescription">
					<b>Localization:</b> We will target keywords like Virginia Pool Services or surrounding areas.  First the home town, then the known area like (DC Metro Area). Then county. Then State. We may also be expanding into the commercial service industry.
				</p>
				<p class="cSuiteCaseStudyParagraphDescription">
					<b>Expand the Content:</b> Current content is lacking some key details of the services being offered and also needs to expand how the procedures take place and how long they take. Need professional photos or buy stock photos.
				</p>
				<p>
					The content road map will involve breaking apart current content into separate containers. We're looking to expand the content into 30-60 new pages of content to capture number one listings for the terms that locals are searching.
				</p>
			</div>
		</div>
		<div class="col-sm-12 col-md-6 col-lg-6">
			<div class="cSuiteLetterTitleShell" style="font-size: .8em; margin-bottom: 0;">
				<div class="cSuiteLetterTitleLetter">
					S
				</div>
				<div class="cSuiteLetterTitle">
					<div class="cSuiteLetterTitleTop">
						The
					</div>
					<div class="cSuiteLetterTitleBottom">
						Strategy
					</div>
				</div>
				<br /><br /><br /><br /><br /><br />
				<h4>Pay Per Click Budget: $250 Recommended</h4>
				<p class="cSuiteCaseStudyParagraphDescription">
					Strategy should be to atttract local audiences. Small radius < 1 mile.
				</p>
				<h4>Facebook Ads Budget: $250 Recommended</h4>
				<p class="cSuiteCaseStudyParagraphDescription">
					I think the best areas to focus for Facebook Ads would be:
				</p>
				<ul class="cSuiteCaseStudyParagraphDescription">
					<li>above ground pool cleaning service</li>
					<li>inground pool service</li>
					<li>monthly pool service<li>
					<li>pool opening service</li>
					<li>salt water pool maintenance</li>
					<li>one time pool cleaning</li>
				</ul>
				<p class="cSuiteCaseStudyParagraphDescription">
					Reason being, it should be easy to target the appropriate demographics. And these are the services that people might want to see pop up in Facebook and they click.	
				</p>
			</div>
		</div>
	</div>
</div>
<div class="container cSuiteHeaderPadding">
	<h1 class="cSuiteMainTitle" style="font-weight: normal;">
		Let's Discuss Your <br /><b>Digital Marketing Leadership</b><br />Needs
	</h1>
	<div class="cSuiteGrowButtonShell">
		<a href="./contactCSuite.php" class="kulaLargerTitle cSuiteGrowButton" style="font-weight: normal; text-align: center;">
			Start Here
		</a>
	</div>
</div>
<?php
	require_once("../tehPHP/kulaFooter.php")
?>