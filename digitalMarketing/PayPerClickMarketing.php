<?php
	$pageTitle = "Fractional C-Suite | Fractional Digital Marketing Services - Pay Per Click Marketing";
	$pageKeywords = "arlington business intelligence services, arlington virginia business intelligence services, arlington va business intelligence services";
	$pageDesc = "With over 25 years of experience, explore our Business Intelligence Services or speak with one of our technical consultants, we'll find a solution that fits your needs and budget.";


	require_once("../tehPHP/kulaHeader.php");
?>

<style>
	.stdBoxShadow{box-shadow: 20px 20px 80px #ABABAB;}

	.cSuiteContentWrapper{max-width: 1100px; margin: auto;}
	.cSuiteContentShell{padding: 50px 5%;}

	.cSuiteMainContentTitle{font-size: 3em;}
	.cSuiteMainContentTitleUnderline{border-top: 1px solid #292b2c;}

	.floatImageRight{float: right;}
	.imageMaxWidth300{max-width: 500px;}
	.cSuiteContentImage{padding: 3px; border: 1px solid #CDCDCD; margin: 20px;}
</style>

<div class="cSuiteContentWrapper">
	<div class="cSuiteContentShell stdBoxShadow">
		<div class="cSuiteMainContentTitle cSuiteOffBlackText cSuiteBlueText boldFont">
			Pay Per Click Marketing
		</div>
		<img title="" class="cSuiteContentImage floatImageRight imageMaxWidth300" src="<?php echo $tehAbsoluteURL; ?>layout/digitalMarketing/whatArePayPerClickAds.jpg" alt="Fractional Pay Per Click Marketing" />
		<div class="cSuiteMainContentSubTitle cSuiteOffBlackText">
			Fractional Pay Per Click Marketing Leadership &amp; Executive Services
		</div>
		<br />
		<p>
			Pay-Per-Click (PPC) advertising is a method of advertising in search engines that <b>guarantees first page listings</b> across Google, Yahoo, and Bing. PPC is an essential piece to any successful digital marketing campaign. Even the smallest of budgets offer tremendous value.
		</p>
		<div class="cSuiteContentParagraphTitle boldFont">
			What is Pay Per Click (PPC)
		</div>
		<p>
			Pay Per Click marketing is a digital marketing strategy created by search engines like Google, Yahoo, and Bing (Microsoft) where businesses with a website may bid on search terms in order to appear at the top of search results.
		</p>
		<p>
			Each of the major search engines offer their own PPC service. The free service comes with terrific tools, reports, and analytics to help manage all pay-per-click campaigns.
		</p>
		<div class="row">
			<div class="col-sm-12 col-md-4 col-lg-4">
				<img width="100%" alt="Facebook Ads" src="<?php echo $tehAbsoluteURL; ?>layout/digitalMarketing/facebookAdsLogo.png" />
			</div>
			<div class="col-sm-12 col-md-4 col-lg-4">
				<img width="100%" alt="Google Adwords" src="<?php echo $tehAbsoluteURL; ?>layout/digitalMarketing/googleAdwordsLogo.jpg" />
			</div>
			<div class="col-sm-12 col-md-4 col-lg-4">
				<img width="100%" alt="Yahoo Network Ads" src="<?php echo $tehAbsoluteURL; ?>layout/digitalMarketing/yahooNetworkLogo.jpg" />
			</div>
		</div>
		<br />
		<div class="cSuiteContentParagraphTitle boldFont">
			How does PPC work?
		</div>
		<p>
			Search engines like Yahoo, Bing, and Google offer a free to use PPC service, each with it's own unique platform to help businesses manage PPC campaigns.
		</p>
		<p>Key Terms:
			<li>Pay Per Click: PPC</li>
			<li>
		</p>
		<p>
			Each search engine offers their own unique platform with plenty of free training but the root of each platform is the same. Keywords are placed up for auction to the highest bidder.
		</p>
		<p>
			Key search terms are placed up for auction to the highest bidder by each of the search engine platforms and the highest bidding, website owners will be placed at the top of the sponsored listings generally marked as "Ads" somewhere in the listing.
		</p>

			With a simple account creating and a quick website owner validation,  search terms relevant to results of the major search engines like Google, Yahoo, and Facebook. 


			of search term results. Put simply, website owners may bid to rank number 1-5 in Google. You may have come across these terms while performing your own internet searches
			Each search engine owns their own platform where website owners may bid on search terms to rank in the search engines.
		</p>
		<p>
			Every effective digital marketing campaign includes an effective Pay Per Click strategy as one piece to the whole pie. 
		</p>
		A proper strategy will have measurable metrics and goals.

	</div>
</div>