<?php
	$pageTitle = "Fractional C-Suite | Digital Marketing Case Study: Revtech Performance";
	$pageKeywords = "digital marketing case study revtech performance";
	$pageDesc = "A look at the digital marketing work Fractional-Csuite Performed for client: Revtech Performance.";


	require_once("../tehPHP/kulaHeader.php");
?>
<script src="../js/jquery.ez-bg-resize.js" type="text/javascript" charset="utf-8"></script>
<script>
	$(document).ready(function() {
		
	});
</script>
<style>
	.revTechCaseStudyBG{background-image: url(../layout/backgrounds/revtechCaseStudyBG.jpg);}
	.cSuiteBGDiv{display: -webkit-flex;display: -ms-flexbox;display: flex;-webkit-flex-direction: column;-ms-flex-direction: column;flex-direction: column;-webkit-justify-content: middle;-ms-flex-pack: middle;justify-content: middle;-webkit-align-items: center;-ms-flex-align: center;align-items: center;width: 100%;min-height: 700px;vertical-align: middle;padding-top: 5%;padding-bottom: 5%;background-size: cover;background-repeat: no-repeat;background-position: 75% 75%;position: relative;}
	.cSuiteBGDiv:before, .cSuiteBGDiv:after{box-sizing: border-box;}
	.cSuteCaseStudyLogoShell{text-align: center;}
	.cSuteCaseStudyLogo{max-width: 350px;}
	.cSuteCaseStudyP{max-width: 800px; color: #FFFFFF; font-size: 1.3em; margin: 40px 0 20px 0; text-align: center;}
	.cSuiteCaseStudyColumnShell{margin: 10px 20px;border: 3px solid white; display: block;}
	.cSuiteCaseStudyColumnTitle{text-transform: uppercase; font-weight: bold; text-align: center;  color: #FFFFFF; }
	.cSuiteCaseStudyTableWrapper{min-width: 300px; max-width: 900px;}
	.cSuiteCaseStudyColumnSubTitle{font-size: 1.1em; color: #FFFFFF; text-align: center;}
	.cSulteCaseStudyList{list-style: none; padding: 0; margin: 0;}
	.cSuiteCaseStudyMacBook{ max-width: 1000px; margin-top: -95px; position: relative; z-index: 5;}
	.cSuiteTextAlignCenter{text-align: center;}
	.cSuiteCaseStudyParagraphDescription{font-size: 1.4em;}
	.cSuiteImage100{width: 100%;}
	.cSuiteRevTechNoResults{margin-top: 180px; border-top: 1px solid #CDCDCD; border-left: 1px solid #CDCDCD;}
	.cSuiteGradientBG{padding: 60px 20px; min-height: 400px; width: 100%; background-image: linear-gradient(to right bottom, #18a3c4, #1f8faf, #237b9a, #236884, #22566f);}
	.cSuiteCaseStudyResultsParagraph{font-size: 1.1em; color: #FFFFFF;}

	@media screen and (max-width: 767px) {
		.cSuiteRevTechNoResults{margin-top: 0;}
		.cSuiteResultsBuffer{margin-top: 150px;}
			}
</style>
<div class="cSuiteBGDiv revTechCaseStudyBG">
	<div class="cSuteCaseStudyLogoShell">
		<img class="cSuteCaseStudyLogo" alt="Revtech Performance" src="<?php echo $tehAbsoluteURL; ?>/layout/logos/revTechLogo.png" />
	</div>
	<p class="cSuteCaseStudyP">
		Armed with over 25 years of experience Revtech's certified technicians deliver Racing and Performance enhancments to vehicles in Sterling, Virginia.  Revtech's state of the art garage let's your dream car become realtity.
	</p>
	<div class="cSuiteWrapper cSuiteCaseStudyListShell">
		<div class="row">
			<div class="col-sm-12 col-md-4 col-lg-4">
				<ul class="cSuiteLeadershipServicesList">
					<div class="cSuiteCaseStudyColumnTitle">
						Industry
					</div>
					<div class="cSuiteCaseStudyColumnSubTitle">
						Racing Shop
					</div>
				</ul>
			</div>
			<div class="col-sm-12 col-md-4 col-lg-4">
				<ul class="cSuiteLeadershipServicesList">
					<div class="cSuiteCaseStudyColumnTitle">
						Strategy
					</div>
					<div class="cSuiteCaseStudyColumnSubTitle">
						<ul class="cSulteCaseStudyList">
							<li>Pay Per Click</li>
							<li>Organic Keyword Targetting</li>
							<li>Content Writing</li>
							<li>Social Media</li>
						</ul>
					</div>
				</ul>
			</div>
			<div class="col-sm-12 col-md-4 col-lg-4">
				<ul class="cSuiteLeadershipServicesList">
					<div class="cSuiteCaseStudyColumnTitle">
						Location
					</div>
					<div class="cSuiteCaseStudyColumnSubTitle">
						Sterling, VA
					</div>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="cSuiteWrapper cSuiteTextAlignCenter">
	<img  class="cSuiteCaseStudyMacBook" alt="RevTech Design" src="<?php echo $tehAbsoluteURL; ?>/layout/images/revTechCaseStudyMacBook.png"/>
</div>
<div class="cSuiteWrapper">
	<div class="row">
		<div class="col-sm-12 col-md-6 col-lg-6">
			<div class="cSuiteLetterTitleShell" style="font-size: .8em; margin-bottom: 0;">
				<div class="cSuiteLetterTitleLetter">
					C
				</div>
				<div class="cSuiteLetterTitle">
					<div class="cSuiteLetterTitleTop">
						The
					</div>
					<div class="cSuiteLetterTitleBottom">
						Challenges
					</div>
				</div>
				<br /><br /><br /><br /><br /><br />
				<p class="cSuiteCaseStudyParagraphDescription">
					Revtech faced stiff competition as the new kid on the block with several well-established automotive shops in the area and they had a templated site set up by a friend of a friend.
				</p>
				<p class="cSuiteCaseStudyParagraphDescription">
					Revtech had no real knowledge of Digital Marketing and no strategy. They needed true digital marketing executive leadership to fill the void and get them on track to perform in the search engines both Organically and Paid Advertising routes.
				</p>
			</div>
		</div>
		<div class="col-sm-12 col-md-6 col-lg-6">
			<div class="cSuiteTextAlignCenter">
				<img  class="cSuiteImage100 cSuiteRevTechNoResults" alt="RevTech NoResults" src="<?php echo $tehAbsoluteURL; ?>/layout/images/noResults.png"/>
			</div>
		</div>
	</div>
</div>
<div class="cSuiteWrapper">
	<div class="row">
		<div class="col-sm-12 col-md-6 col-lg-6">
			<div class="cSuiteTextAlignCenter">
				<img  class="cSuiteImage100 cSuiteRevTechNoResults" alt="RevTech NoResults" src="<?php echo $tehAbsoluteURL; ?>/layout/images/noResults.png"/>
			</div>
		</div>
		<div class="col-sm-12 col-md-6 col-lg-6">
			<div class="cSuiteLetterTitleShell" style="font-size: .8em; margin-bottom: 0;">
				<div class="cSuiteLetterTitleLetter">
					S
				</div>
				<div class="cSuiteLetterTitle">
					<div class="cSuiteLetterTitleTop">
						The
					</div>
					<div class="cSuiteLetterTitleBottom">
						Strategy
					</div>
				</div>
				<br /><br /><br /><br /><br /><br />
				<p class="cSuiteCaseStudyParagraphDescription">
					Thoughtful <b>Pay Per Click Strategy</b> with only a budget of $150 a month played a key roll in letting the public know about this new Racing Shop of Sterling Virginia.
				</p>
				<p class="cSuiteCaseStudyParagraphDescription">
					The real success story here is Fractional's <b>Content Writing</b> and <b>Organic Keyword Targetting</b>. With the content Fractional delivered, Revtech Performance reached the front page of Google for over 15 different keyword phrases and pages. Organic traffic alone is responsible for over 66$ of traffic generated leading to a 4-9% lead generation off those visitors.
				</p>
				<p class="cSuiteCaseStudyParagraphDescription">
					Social marketing campaigns have also yielded a 1-7% conversion rate consistently over the last 4 quarters.
				</p>
			</div>
		</div>
	</div>
</div>
<div class="cSuiteGradientBG">
	<div class="cSuiteWrapper">
		<div class="row">
			<div class="col-sm-12 col-md-5 col-lg-5">
				<div class="cSuiteLetterTitleLetter" style="font-color: #FFFFFF; margin-bottom: 0;">
					R
				</div>
				<div class="cSuiteLetterTitle">
					<div class="cSuiteLetterTitleTop">
						The
					</div>
					<div class="cSuiteLetterTitleBottom">
						Results
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-7 col-lg-7">
				<div class="row cSuiteResultsBuffer" style="font-size: 1.2em;">
					<div class="col-sm-4 col-md-4 col-lg-4">
						<ul class="cSuiteLeadershipServicesList">
							<div class="cSuiteCaseStudyColumnTitle">
								Traffic Growth
							</div>
							<div class="cSuiteCaseStudyColumnSubTitle">
								~ 1,500 visits/month
							</div>
						</ul>
					</div>
					<div class="col-sm-4 col-md-4 col-lg-4">
						<ul class="cSuiteLeadershipServicesList">
							<div class="cSuiteCaseStudyColumnTitle">
								Lead Generation
							</div>
							<div class="cSuiteCaseStudyColumnSubTitle">
								4-12%
							</div>
						</ul>
					</div>
					<div class="col-sm-4 col-md-4 col-lg-4">
						<ul class="cSuiteLeadershipServicesList">
							<div class="cSuiteCaseStudyColumnTitle">
								ROI
							</div>
							<div class="cSuiteCaseStudyColumnSubTitle">
								3-600%
							</div>
						</ul>
					</div>
				</div>
				<p class="cSuiteCaseStudyResultsParagraph">
					Fractional's digital marketing leadership and strategy boosted Revtech to the top of the search engines both organically and with paid advertising. With an average ROI of about 450% lead generation cost is down to a minimal thanks to the Organic Keyword Targetting.
				</p>
				<p class="cSuiteCaseStudyResultsParagraph">
					Traffic numbers continue to increase month to month while revenues continue to climb.
				</p>
			</div>
		</div>
	</div>
</div>
<br /><br /><br />
<div class="cSuiteTextAlignCenter">
	<img class="cSuiteBoxShadow cSuiteCaseStudyImage" title="" alt="" src="<?php echo $tehAbsoluteURL; ?>/layout/digitalMarketing/caseStudies/revTechMarketingResults.png" />
</div>
<style>
.cSuiteCaseStudyImage{padding: 5px; border: 1px solid #CDCDCD;}
.cSuiteBoxShadow{box-shadow: 20px 20px 80px #ABABAB;}
</style>
<div class="container cSuiteHeaderPadding">
	<h1 class="cSuiteMainTitle" style="font-weight: normal;">
		Let's Discuss Your <br /><b>Digital Marketing Leadership</b><br />Needs
	</h1>
	<div class="cSuiteGrowButtonShell">
		<a href="./contactCSuite.php" class="kulaLargerTitle cSuiteGrowButton" style="font-weight: normal; text-align: center;">
			Start Here
		</a>
	</div>
</div>
<?php
	require_once("../tehPHP/kulaFooter.php")
?>