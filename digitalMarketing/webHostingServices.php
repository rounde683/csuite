<?php
	$pageTitle = "Fractional C-Suite | Web Hosting Packages - Services";
	$pageKeywords = "";
	$pageDesc = "";


	require_once("../tehPHP/kulaHeader.php");
?> 

<div class="cSuiteContentWrapper">
	<div class="cSuiteContentShell stdBoxShadow stdLargeText">
		<div class="cSuiteMainContentTitle cSuiteOffBlackText cSuiteBlueText boldText">
			Web Hosting Packages - Services
		</div>
		<div class="cSuiteMainContentSubTitle cSuiteOffBlackText stdLargerText">
			Fractional Organic Keyword Targeting Services
		</div>
		<br />
		<p>
			Website hosting is one of the only small costs to website owners.
		</p>
		<div class="row">
			<div class="col-sm-12 col-md-4 col-lg-4">
				Free
			</div>
			<div class="col-sm-12 col-md-4 col-lg-4">
				Standard
			</div>
			<div class="col-sm-12 col-md-4 col-lg-4">
				Platinum
			</div>
		</div>
		<br /><br /><br /><br /><br /><br /><br /><br /><br />
		<br /><br /><br /><br /><br /><br /><br /><br /><br />
		<br style="clear: both;" />
		<div class="cSuiteContentParagraphTitle boldText cSuiteBlueText stdLargerText">
			Fractional PPC Management Services
		</div>
		<div class="row">
			<div class="col-sm-12 col-md-6 col-lg-6">
				<div class="cSuiteColumnIconShell centerText">
					<img width="100" title="Fractional Fully Managed PPC Services" alt="Fully Managed PPC Services" src="<?php echo $tehAbsoluteURL; ?>layout/digitalMarketing/managedPPCIcon.png" />
					<div class="cSuiteColumnIconTitle cSuiteBlueText stdLargeText boldText">
						Fully Managed PPC Services
					</div>
					<ul class="cSuiteBlueText squareBullets leftText">
						<li>Aligned to Business Goals</li>
						<li>Results Driven</li>
						<li>Meetings</li>
					</ul>
				</div>
			</div>
			<div class="col-sm-12 col-md-6 col-lg-6">
				<div class="cSuiteColumnIconShell centerText">
					<img width="100" title="Fractional PPC Coaching &amp; Consulting Services" alt="PPC Coaching &amp; Consulting Services" src="<?php echo $tehAbsoluteURL; ?>layout/digitalMarketing/coachingPPC.png" />
					<div class="cSuiteColumnIconTitle cSuiteBlueText stdLargeText boldText">
						PPC Coaching &amp; Consulting Services
					</div>
					<ul class="cSuiteBlueText squareBullets leftText">
						<li>Aligned to Business Goals</li>
						<li>Results Driven</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="cSuiteContentParagraphTitle boldText cSuiteBlueText stdLargerText">
			Why Pay Per Click (PPC)?
		</div>
		<p class="stdLargeText">
			Pay per click marketing is a powerful solution that drives revenue. It aids online brand recognition and expands online presence. It's an essential piece to an effective digital marketing campaign.
		</p>
		<br />
		<div class="cSuiteContentIndentShell">
			<div class="cSuiteColumnTitle stdLargerText">
				PPC Contributes to Business &amp; Digital Marketing Goals
			</div>
			<p>
				Is the current digital marketing goal to increase visitors and sales? Is it to improve brand recognition and online presence? Or is it to suffocate the competition? PPC may be aligned to meet these scenarios and more.  PPC goals should be clearly defined from the onset so that measurables may be put in place to keep the PPC campaigns aligned to meet the business need.
			</p>
			<div class="cSuiteColumnTitle stdLargerText">
				PPC Drives Revenue &amp; Brand Recognition
			</div>
			<p class="cSuiteOffBlackText">
				PPC allows businesses to reach targeted customers searching directly for your products or services. Targeted ads quickly educate visitors leading to conversions and sales that affect the bottom line - revenue. Brick and mortor stores equally benefit from the brand recognition that search engine ads provide.
			</p>
			<div class="cSuiteColumnTitle stdLargerText">
				PPC Results are Easily Measurable
			</div>
			<p class="cSuiteOffBlackText">
				Return on advertising metrics are just the tip of the iceberg, All PPC platforms offer extensive tools to gather metrics, run reports, and generate data to be analyzed. Sales may be traced all the way back to key terms and sucessful ads that led to the conversion.
			</p>
			<div class="cSuiteColumnTitle stdLargerText">
				PPC Data Sheds Light into other Digital Marketing Channels
			</div>
			<p class="cSuiteOffBlackText">
				The data gathered by Google Adwords is incredible and it may lead to other digital marketing avenues based on what the data suggests. Often times it's the unexpected keyword terms that perform better than other obvious choices. Additionally, PPC data may light the path toward Organic Keyword Targeting based on conversion numbers.
			</p>
			<div class="row">
				<div class="col-sm-12 col-md-6 col-lg-6">
					<ul class="squareBullets">
						<li>Name your own Marketing Budget</li>
						<li>Demographics are highly targetable</li>
						<li>Guaranteed visitors</li>
					</ul>
				</div>
				<div class="col-sm-12 col-md-6 col-lg-6">
					<ul class="squareBullets">
						<li>Generate visitors quickly</li>
						<li>Provides dditional insights into visitor behavior</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
	<div class="cSuiteBigBlueBG">
		<div class="cSuiteContentWrapper">
		</div>
	</div>


		

	<div class="cSuiteContentWrapper">
		<div class="cSuitePreFooterActionWindow">
			<h1 class="cSuitePreFooterActionTitle">
				Leverage Our PPC Leadership &amp; Management Services for your Business
			</h1>
			<br />
			<h6 class="cSuitePreFooterQuote">
				"The essence of strategy is choosing what <b><i><u>not</u></i></b> to do." -- Michael Porter (Harvard Business School)
			</h6>
			<div class="cSuiteGrowButtonShell">
				<a href="./gettingStartedWithFractional.php" class="kulaLargerTitle cSuiteGrowButton" style="font-weight: normal; text-align: center;">
					Request a Proposal
				</a>
			</div>
		</div>

	</div>
	<br /><br /><br />
</div>
<?php
	require_once("../tehPHP/kulaFooter.php")
?>