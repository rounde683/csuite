<?php
	$pageTitle = "CMB Networks | Arlington VA, Business IntelligenceServices";
	$pageKeywords = "arlington business intelligence services, arlington virginia business intelligence services, arlington va business intelligence services";
	$pageDesc = "With over 25 years of experience, explore our Business Intelligence Services or speak with one of our technical consultants, we'll find a solution that fits your needs and budget.";


	require_once("../tehPHP/kulaHeader.php");
?>


<style>
	.cSuiteContentWrapper{max-width: 1100px; margin: auto;}
	.cSuiteLargerTitle{color: #18A3C4; font-size: 2em;}
	.fullImage{width: 100%;}
	.cSuiteColumnShell{padding: 20px;}
	.cSuiteLogoTextAdjustment{position: relative; top: -80px; font-size: 3em;}




	.cSuiteBigBlueBG{background: #22566F;padding: 100px; min-height: 400px;}
	
	.cSuiteContentGrowButtonShell>a{margin: 10px auto; border: 5px solid #18A3C4; color: #18A3C4; font-weight: bold; display: inline-block; background: #FFFFFF; padding: 15px 30px; transition: all .3s ease; font-size: 1.5em;}
	.cSuiteContentGrowButtonShell>a:hover{padding: 15px 80px; background: #18A3C4; color: #FFFFFF;}

	.cSuiteServiceIconWindow{padding: 100px 0;}
	.cSuiteServiceIconWindowIconShell{padding: 10px 20px;}
	.cSuiteServiceIconWindowIconTitle{font-size: 2em; color: #18A3C4; text-align: center; font-weight: bold;}
	.cSuiteServiceIconWindowIcon{text-align: center; width: 200px;}


</style>
<br /><br /><br /><br />
<div class="">
	<div class="cSuiteContentWrapper">
		<div class="cSuiteLogoWindow cSuiteColumnShell">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<img class="fullImage" src="<?php echo $tehAbsoluteURL; ?>layout/logos/fractionCSuiteLogo.png" alt=""/>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="cSuiteLargerTitle cSuiteLogoTextAdjustment">
						Digital Marketing Executive Services
					</div>
				</div>
			</div>
		</div>
		<div class="cSuiteServiceIconWindow">
			<div class="row">
				<div class="col-md-4 col-sm-4">
					<div class="cSuiteServiceIconWindowIconShell">
						<center>
							<img class="cSuiteServiceIconWindowIcon" src="<?php echo $tehAbsoluteURL; ?>layout/icons/businessIntelligenceIcon-lb.png" alt=""/>
						</center>
						<div class="cSuiteServiceIconWindowIconTitle">
							Analytics
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="cSuiteServiceIconWindowIconShell">
						<center>
							<img class="cSuiteServiceIconWindowIcon" src="<?php echo $tehAbsoluteURL; ?>layout/icons/trainingIcon.png" alt=""/>
						</center>
						<div class="cSuiteServiceIconWindowIconTitle">
							Training
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="cSuiteServiceIconWindowIconShell">
						<center>
							<img class="cSuiteServiceIconWindowIcon" src="<?php echo $tehAbsoluteURL; ?>layout/icons/legalComplianceIcon.png" alt=""/>
						</center>
						<div class="cSuiteServiceIconWindowIconTitle">
							Strategy
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	<br /><br /><br /><br /><br /><br />

	<div class="cSuiteBigBlueBG">
		<div class="cSuiteContentWrapper">
		</div>
	</div>


		

	<div class="cSuiteContentWrapper">
		<div class="cSuitePreFooterActionWindow">
			<h1 class="cSuitePreFooterActionTitle">
				Let Our Executive Expertise take your business to the next level
			</h1>
			<br />
			<h6 class="cSuitePreFooterQuote">
				"The essence of strategy is choosing what <b><i><u>not</u></i></b> to do." -- Michael Porter (Harvard Business School)
			</h6>
			<div class="cSuiteCenterMe cSuiteContentGrowButtonShell">
				<a href="<?php echo $tehAbsoluteURL;?>">
					Get Started - Here
				</a>
			</div>
		</div>

	</div>
	<br /><br /><br />
</div>
<?php
	require_once("../tehPHP/kulaFooter.php")
?>