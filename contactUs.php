<?php
	$pageTitle = "CMB Networks | Contact Us, An Arlington VA, Technical Consulting and Infrastructure Architecting";
	$pageKeywords = "contact technical consulting, contact technical consultant,contact infrastructure architect,contact infrastructure architecting";
	$pageDesc = "Contact Arlington Virginia's one stop shop for technical consulting, data security, and infrastructure design and implementation.";


	require_once("tehPHP/kulaHeader.php");
?>
<script src="js/jquery.ez-bg-resize.js" type="text/javascript" charset="utf-8"></script>
<script src="js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.validationEngine-2.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="css/validationEngine.jquery.css" />


<script>
	function beforeCall(form, options)
	{
		if (window.console) 
		console.log("Right before the AJAX form validation call");
		return true;
	}
		
	function ajaxValidationCallback(status, form, json, options)
	{
		if (window.console) 
		console.log(status);
		if (status === true)
		{
			var successMessage = "<div><h1>Thank You</h1><p>We have received your inquiry and we will be contacting you shortly.</p></div>";
			$("#contactFormShell").html(successMessage);
		}
	}
		
	$(document).ready(function() {
		$("#contactUsForm").validationEngine({
			ajaxFormValidation: true,
			promptPosition: "topLeft",
			scroll: false,
			onBeforeAjaxFormValidation: beforeCall,
			onAjaxFormComplete: ajaxValidationCallback
		});
		$(".mainIndexImage").ezBgResize({
			img     : "./layout/backgrounds/internetConsultantBGW.jpg", // Relative path example.  You could also use an absolute url (http://...).
			opacity : 1, // Opacity. 1 = 100%.  This is optional.
			center  : true // Boolean (true or false). This is optional. Default is true.
		});
	});

</script>

<style>
		.kulaContactFormShell{margin-top: -150px !important;}
		.fiftyShell{width: 50%; float: left; margin: 8px 0;}
		.fiftyShellInputShellMarginRight{margin-right: 10px;}
		.fiftyShellInputShellMarginLeft{margin-left: 10px;}
		.contactUsFormInput{width: 99%; display: block;  box-sizing: border-box;}
		.contactTextArea{width: 100%; box-sizing: border-box; margin: 8px 0; height: 150px;}
		.contactUsFormTitle{text-align: left; color: #45A6FC; padding-bottom: 20px;}
		.kulaContactFormSocialShell{position: relative; top: 40px; float: right; background: #04325A; padding: 20px 15px; width: 25px;}
		.kulaContactFormSocialList{list-style: none; margin: 0; padding: 0; color: #FFFFFF;}
		.kulaContactFormSocialList>li{padding: 7px 0;}
		.kulaContactFormSocialList>li>a{text-decoration: none; color: #FFFFFF; font-size: 2em;}
		.kulaContactFormSocialList>li>a:hover{color: #45A6FC;}
		.kulaContactFormShell{max-width: 600px; margin: auto; position: relative;}
		.contactUsFormShell{background: #FFFFFF; padding: 40px 40px 20px 40px; border: 1px solid #EFEFEF; box-shadow: 3px 3px 5px #CDCDCD; max-width: 463px; margin: 60px auto;}
		button{background: none !important; border: none !important;}
		button:hover{cursor: pointer;}
		input,select.contactUsFormInput,.contactTextArea{border: 1px solid #FFFFFF;outline: none;padding: 10px;color: #989898;font-size: .8em;background: #EFEFEF;-webkit-border-radius: 3px;-khtml-border-radius: 3px;border-radius: 3px;}
		
		.lessonFooterShell{padding: 20px 40px; color: #ABABAB; height: 40px; clear: both;}
		@media only screen and ( max-width: 680px)
		{
			.kulaContactFormSocialShell{display: none;}
		}
		@media only screen and ( max-width: 500px)
		{
			.fiftyShell{float: none; width: 100%;}
			.fiftyShellInputShellMarginRight{margin: 0;}
			.fiftyShellInputShellMarginLeft{margin: 0;}
		}
	</style>
<div class="mainIndexImage">
	<div class="container kulaHeaderPadding">
		<div class="row">
			<div class="col-md-12 kulaKeaderShell">
				<img src="./layout/logos/CMBLogo.png" alt="Arlington Virginia Technical Consluting Logo" width="100%" />
			</div>
		</div>
	</div>
	<div class="contactUsFormShell" id="contactFormShell">
		<form id="contactUsForm" class="form-horizontal" role="form" method="POST" action="tehPHP/validations/contactUsFormValidation.php">
			<div class="contactUsFormTitle">
				Send Us a Message
			</div>
			<div class="fiftyShell">
				<div class="fiftyShellInputShellMarginRight">
					<input type="name" class="contactUsFormInput validate[required] text-input" name="contactNameField" id="contactNameField" placeholder="Name">
				</div>
			</div>
			<div class="fiftyShell">
				<div class="fiftyShellInputShellMarginLeft">
					<input type="email" class="contactUsFormInput validate[required,custom[email]] text-input" name="contactEmailField" id="contactEmailField" placeholder="Email">
				</div>
			</div>
			<div class="fiftyShell">
				<div class="fiftyShellInputShellMarginRight">
					<input type="name" class="contactUsFormInput validate[required] text-input" name="contactCompanyNameField" id="contactCompanyNameField" placeholder="Company Name">
				</div>
			</div>
			<div class="fiftyShell">
				<div class="fiftyShellInputShellMarginLeft">
					<input type="phone" class="contactUsFormInput validate[required,custom[phone]] text-input" name="contactPhoneField" id="contactPhoneField" placeholder="Phone">
				</div>
			</div>
			<div class="paddingFix">
				<textarea name="contactMessageField"  id="contactMessageField" class="validate[required] text-input contactTextArea form-control contactTextArea" placeholder="Message"></textarea>
			</div>
			<br />
			<center>
				<button type="submit" class="kulaGrowButtonShell">
					<a class="growButtonRedefine">
						Send
					</a>
				</button>
			</center>
		</form>
	</div>
</div>
<?php
	require_once("tehPHP/kulaFooter.php")
?>