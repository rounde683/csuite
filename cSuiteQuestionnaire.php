<?php
	$pageTitle = "Fractional C-Suite | Leadership Questionnaire";
	$pageKeywords = "";
	$pageDesc = "";


	require_once("tehPHP/kulaHeader.php");
?>
<script>
	$(document).ready(function() {
		$(".cSuiteQuestionnareIconShell").click(function() {

		
			var whatForm = $(this).data('form');
			var whatFieldSet = $(this).data('fieldset');

			if($(this).hasClass("cSuilteQuestionnaireIconActive"))
			{
				//remove form fields
				if(confirm("Some of your form data will be reset. Is this OK?"))
				{
					$('.'+whatFieldSet).html('');
					$(this).toggleClass('cSuilteQuestionnaireIconActive');
				}
			}
			else	
			{
				//add form fields
				var addFields = $('.'+whatForm).html();
				
				//console.log(addFields);
				$('.'+whatFieldSet).html(addFields);
				$(this).toggleClass('cSuilteQuestionnaireIconActive');
				
			}
  		});
	});
</script>
<style>
	.cSuiteFormTitleTitle{color: #22566F; font-size: 2em; font-weight: bold;}
	.cSuiteNDAField{font-size: 1.5em !important; line-height: 1.5em; height: 60px !important; position: relative !important;}
	.cSuiteQuestionnaireFieldTitle{color: #22566F; font-weight: bold;}

	.cSuiteServiceIconWindow{padding: 100px 0;}
	.cSuiteQuestionnareIconShell{padding: 60px 20px; display: block; border-radius: 5px;}
	.cSuiteQuestionnareIconTitle{font-size: 1.2em; color: #22566F; text-align: center; font-weight: bold;}
	.cSuiteQuestionnareIcon{text-align: center; width: 100px;}

	.cSuilteQuestionnaireIconActive{background: #D3F1F8;}

	.cSuiteQuestionnaireFromWrapper{max-width: 800px;}

	.cSuiteDataSecuritySecretForm{display: none;}
	.cSuiteCorporateCounselSecretForm{display: none;}
	.cSuiteDigitalMarketingSecretForm{display: none;}

	.cSuiteQuestionnaireTextArea{min-height: 100px;}
	.cSuiteQuestionnaireFieldTitle>span{font-weight: normal;}

	/*cSuilteQuestionnaireIconActive*/

</style>
<div class="cSuiteWrapper">
	<h1 class="cSuiteMainTitle">
		Fractional C-Suite Questionnaire
	</h1>
</div>
<form id="cSuiteQuestionnaireForm" method="POST" action="<?php echo $tehAbsoluteURL; ?>tehPHP/validations/contactUsFormValidation.php">
	<fieldset>
		<div class="cSuiteWrapper cSuiteQuestionnaireFromWrapper">
			<div class="cSuiteFormTitleTitle" style="font-size: 1.3em; text-align: center;position: relative; top: 100px;">
				Please select (click) the Fracitional Leadership Services you're intersted in:
			</div>
			<div class="cSuiteServiceIconWindow">
				<div class="row">
					<div class="col-md-4 col-sm-4">
						<a href="javascript:void(0)" class="cSuiteQuestionnareIconShell" data-fieldset="cSuiteCorporateCounselFieldSet" data-form="cSuiteCorporateCounselSecretForm">
							<center>
								<img class="cSuiteQuestionnareIcon" src="<?php echo $tehAbsoluteURL; ?>layout/icons/corporateCounselIcon.png" alt=""/>
							</center>
							<div class="cSuiteQuestionnareIconTitle">
								Corporate Counsel
							</div>
						</a>
					</div>
					<div class="col-md-4 col-sm-4">
						<a href="javascript:void(0)" class="cSuiteQuestionnareIconShell" data-fieldset="cSuiteDataSecurityFieldSet" data-form="cSuiteDataSecuritySecretForm">
							<center>
								<img class="cSuiteQuestionnareIcon" src="<?php echo $tehAbsoluteURL; ?>layout/icons/dataSecurityIcon.png" alt=""/>
							</center>
							<div class="cSuiteQuestionnareIconTitle">
								IT &amp; Data Security
							</div>
						</a>
					</div>
					<div class="col-md-4 col-sm-4">
						<a href="javascript:void(0)" class="cSuiteQuestionnareIconShell" data-fieldset="cSuiteDigitalMarketingFieldSet" data-form="cSuiteDigitalMarketingSecretForm">
							<center>
								<img class="cSuiteQuestionnareIcon" src="<?php echo $tehAbsoluteURL; ?>layout/icons/digitalMarketing.png" alt=""/>
							</center>
							<div class="cSuiteQuestionnareIconTitle">
								Digital Marketing
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="cSuteMainTextBannerShell" style="background: #F8F8F8;">
			<div class="cSuiteWrapper cSuiteQuestionnaireFromWrapper">
				<div class="cSuiteFormTitleTitle">
					Leadership Questionnaire
				</div>
				<p>
					Below is a questionnaire for you to complete. The information provided will be basis for our preliminary assessment and will set the stage for our follow-up consultation call. Since our advice is fractional in nature, we acknowledge you may not be seeking all of our services from the outset; however, answering all of the questions below will provide us a comprehensive profile of your business. Please do your best to answer as completely and accurately as possible and if in doubt, answer "no," we can redress during our consultation call. 
				</p>
				<div class="cSuiteQuestionnaireFieldTitle">
					Please describe the type of business you are operating <span>- essentially your elevator pitch:</span>
				</div>
				<div class="form-group cSuiteQuestionnaireFieldShell">
					<textarea type="text" name="elevatorPitch" size="6" class="form-control elevatorPitch cSuiteQuestionnaireTextArea" placeholder="Elevator Pitch" data-vindicate="required|format:alphanumeric" />Elevator Pitch</textarea>
				</div>
				<div class="cSuiteQuestionnaireFieldTitle">
					Please provide a link to your current webpage or social media accounts so that we can get better acquainted with your web presence.
				</div>
				<div class="form-group cSuiteQuestionnaireFieldShell">
					<input type="text" name="companyURL" size="6" class="form-control companyURL cSuiteQuestionnaireField" placeholder="Company Website" value="http://www.tizag.com" />
				</div>
				<div class="cSuiteQuestionnaireFieldTitle">
					How many clients/customers are you currently servicing?
				</div>
				<div class="form-group cSuiteQuestionnaireFieldShell">
					<input type="text" name="clientNumber" size="6" class="form-control clientNumber cSuiteQuestionnaireField" placeholder="Number of Clients" value="Number of Clients"/>
				</div>
				<div class="cSuiteQuestionnaireFieldTitle">
					What are you projecting your growth to be in client/customer base to be in the next 6 months?
				</div>
				<div class="form-group cSuiteQuestionnaireFieldShell">
					<input type="text" name="projectedGrowth" size="6" class="form-control projectedGrowth cSuiteQuestionnaireField" placeholder="Projected Company Growth" value="Projected Growth" />
				</div>
				<div class="cSuiteQuestionnaireFieldTitle">
					If applicable, who would you consider to be your top three competitors in your industry?
				</div>
				<div class="form-group cSuiteQuestionnaireFieldShell">
					<textarea type="text" name="topCompetitors" size="6" class="form-control topCompetitors cSuiteQuestionnaireTextArea" placeholder="Top 3 Competitors" />Top 3 Competitors</textarea>
				</div>
				<div class="cSuiteQuestionnaireFieldTitle">
					We would like to know if  your organization past the concept stage, please respond Yes/No to the following:
				</div>
				<ol style="margin: 25px;">
					<li>
						<div class="cSuiteQuestionnaireFieldTitle">
							Has your company been formed as a legal entity?
						</div>
						<div class="form-group cSuiteQuestionnaireFieldShell">
							<div>
								Yes <input checked type="radio" name="legalEntityYesNo" value="0" class="ssform-control legalEntityYesNo cSuiteQuestionnaireField"/> No <input type="radio" value="1" name="legalEntityYesNo" class="form-controls legalEntityYesNo cSuiteQuestionnaireField"/>
							</div>
						</div>
					</li>
					<li>
						<div class="cSuiteQuestionnaireFieldTitle">
							Have all your formation documents been prepared/executed and the initial securities issued? 
						</div>
						<div class="form-group cSuiteQuestionnaireFieldShell">
							<div>
								Yes <input checked type="radio" name="formationDocs" value="0" class="ssform-control formationDocs cSuiteQuestionnaireField"/> No <input type="radio" name="formationDocs" value="1" class="form-controls formationDocs cSuiteQuestionnaireField" />
							</div>
						</div>
					</li>
					<li>
						<div class="cSuiteQuestionnaireFieldTitle">
							Are you looking for assistance with venture capital/financing? 
						</div>
						<div class="form-group cSuiteQuestionnaireFieldShell">
							<div>
								Yes <input checked type="radio" value="0" name="lookingForFinancing" class="ssform-control lookingForFinancing cSuiteQuestionnaireField"/> No <input type="radio" name="lookingForFinancing" value="1" class="form-controls lookingForFinancing cSuiteQuestionnaireField" />
							</div>
						</div>
					</li>
				</ol>

				<!--
				<div class="cSuiteQuestionnaireFieldTitle">
					We would like to know the players of your team, please indicate the current key players and select their current responsibilities:
				</div>
				-->
				<div class="cSuilteQuestionnaireFieldsets">
					<div class="cSuiteCorporateCounselFieldSet">
					</div>
					<div class="cSuiteDataSecurityFieldSet">
					</div>
					<div class="cSuiteDigitalMarketingFieldSet">
					</div>
				</div>
			</div>
		</div>
		
		<div class="cSuteMainTextBannerShell" style="background: #FFFFFF;">
			<div class="cSuiteWrapper cSuiteQuestionnaireFromWrapper">
				<div class="cSuiteFormTitleTitle">
					NDA
				</div>
				<p>
					The best way that our fractional executives can help is if they receive accurate and complete information about your business. We understand the information you may share can be highly sensitive and/or proprietary in nature - for our ears and eyes only. Also, we associate  an intrinsic value to the information and advice we may communicate to you.
				</p>
				<p>
					To protect each other's "crown jewels," we require a mutual non-disclosure agreement to be on-file prior to our response to an online questionnaire. Please take a moment to review the mutual non-disclosure agreement and complete the fields to agree and accept the terms.
				</p>
				<br /><br />
				<div class="cSuiteQuestionnaireFieldTitle" for="companyName">
					<b>Company Name</b> <span>- Needs to be the legal entity name such as "LLC", "INC", "PA", etc.</span>
				</div>
				<div class="form-group cSuiteQuestionnaireFieldShell">
					<input type="text" id="companyName" name="companyName" size="6" class="form-control companyName cSuiteNDAField" placeholder="Company Name" value="Company Name" />
					
				</div>
				<div class="cSuiteQuestionnaireFieldTitle">
					<b>Signed By</b> 
				</div>
				<div class="form-group cSuiteQuestionnaireFieldShell">
					<input type="text" name="signedBy" size="6" class="form-control signedBy cSuiteNDAField" placeholder="Signed By" value="signedBy" />
				</div>
				<div class="cSuiteQuestionnaireFieldTitle">
					<b>Title</b> <span>- As in Job Title or How you relate to the company.</span>
				</div>
				<div class="form-group cSuiteQuestionnaireFieldShell">
					<input type="text" name="jobTitle" size="6" class="form-control jobTitle cSuiteNDAField" placeholder="Title" value="jobTitle" />
				</div>
				<div class="form-group cSuiteQuestionnaireFieldShell checkbox">
					<div>
						<input class="form-controls cSuiteNDAFields" checked type="checkbox" id="IAgree" name="IAgree" value="agree" /> I Agree to the <a href="">Mutual NDA</a>
					</div>
				</div>
				<br style="clear: both;" />
				<center>
					<button type="submit" class="btn btn-primary" name="signup1" value="Sign up">Sign up</button>
				</center>
				<div id="server-results"></div>
			</div>
		</div>
		
	</fieldset>
</form>

<div class="cSuiteCorporateCounselSecretForm">
	<div class="cSuiteFormTitleTitle">
		Corporate Counsel - Legal Services
	</div>
	<p>We would like to assess what your needs are for legal services:</p>
	<div class="cSuiteQuestionnaireFieldTitle">
		Are you currently using an attorney to build your business? If yes, in what capacity do they serve?
	</div>
	<div class="form-group cSuiteQuestionnaireFieldShell">
		<textarea type="text" name="usingAnAtorney" size="6" class="form-control usingAnAtorney cSuiteQuestionnaireTextArea" placeholder="Using an Attorney?" />Yes Attorney</textarea>
	</div>
	<div class="cSuiteQuestionnaireFieldTitle">
		Have you developed standard forms, checklists and/or agreements specifically tailored to your business? If no, please indicate the types of documents you think you may need.
	</div>
	<div class="form-group cSuiteQuestionnaireFieldShell">
		<textarea type="text" name="formsChecklists" size="6" class="form-control formsChecklists cSuiteQuestionnaireTextArea" placeholder="Using Stardized Forms?" />Standard Forms</textarea>
	</div>
	<div class="cSuiteQuestionnaireFieldTitle">
		Do you engage the services of vendors? If so, please indicate they types of services they provide. 
	</div>
	<div class="form-group cSuiteQuestionnaireFieldShell">
		<textarea type="text" name="engageVendors" size="6" class="form-control engageVendors cSuiteQuestionnaireTextArea" placeholder="Do you utilize the services of Vendors?" />Vendors?</textarea>
	</div>
	<div class="cSuiteQuestionnaireFieldTitle">
		If you have a specific issue or risk that makes you lose sleep at night - please indicate your concerns below:
	</div>
	<div class="form-group cSuiteQuestionnaireFieldShell">
		<textarea type="text" name="legalConcerns" size="6" class="form-control legalConcerns cSuiteQuestionnaireTextArea" placeholder="List any Legal Concerns you may have." />Legal Concerns</textarea>
	</div>
</div>




<div class="cSuiteDataSecuritySecretForm">
	<div class="cSuiteFormTitleTitle">
		Information Technology &amp; Data Security
	</div>
	<p>We would like to assess what your needs are for Information Technology &amp; Security services:</p>
	<div class="cSuiteQuestionnaireFieldTitle">
		Are there any statutory or regulatory security frameworks are you obligated to comply with? (PCI-DSS, HIPAA, NIST, SOX, GLBA)?
	</div>
	<div class="form-group cSuiteQuestionnaireFieldShell">
		<div>
			<input checked class="form-controls cSuiteNDAFields" type="checkbox" id="securityFrameworks" name="securityFrameworks[]" value="PCI-DSS" /> PCI-DSS<br />
			<input class="form-controls cSuiteNDAFields" type="checkbox" id="securityFrameworks" name="securityFrameworks[]" value="HIPAA" /> HIPAA<br />
			<input checked class="form-controls cSuiteNDAFields" type="checkbox" id="securityFrameworks" name="securityFrameworks[]" value="NIST" /> NIST<br />
			<input class="form-controls cSuiteNDAFields" type="checkbox" id="securityFrameworks" name="securityFrameworks[]" value="SOX" /> SOX<br />
			<input checked class="form-controls cSuiteNDAFields" type="checkbox" id="securityFrameworks" name="securityFrameworks[]" value="GLBA" /> GLBA<br />
		</div>
	</div>
	<div class="cSuiteQuestionnaireFieldTitle">
		Does your organization have a security program in place?
	</div>
	<div class="form-group cSuiteQuestionnaireFieldShell">
		<div>
			Yes <input value="1" checked type="radio" name="securityProgram" class="ssform-control securityProgram cSuiteQuestionnaireField"/> No <input value="0" type="radio" name="securityProgram" class="form-controls securityProgram cSuiteQuestionnaireField" />
		</div>
	</div>
	<div class="cSuiteQuestionnaireFieldTitle">
		What would you rate the maturity level of this program?
	</div>
	<div class="form-group cSuiteQuestionnaireFieldShell">
		<textarea type="text" name="securityProgramMaturity" class="form-control securityProgramMaturity cSuiteQuestionnaireTextArea" placeholder="Security Program Maturity" />Security Maturity</textarea>
	</div>
	<div class="cSuiteQuestionnaireFieldTitle">
		Has the security program been assessed by an outside 3rd party?
	</div>
	<div class="form-group cSuiteQuestionnaireFieldShell">
		<div>
			Yes <input checked value="1" type="radio" name="securityThirdParty" class="ssform-control securityThirdParty cSuiteQuestionnaireField"/> No <input type="radio" value="0" name="securityThirdParty" class="form-controls securityThirdParty cSuiteQuestionnaireField" />
		</div>
	</div>
	<div class="cSuiteQuestionnaireFieldTitle">
		What are your concerns with your organizations existing security program?
	</div>
	<div class="form-group cSuiteQuestionnaireFieldShell">
		<textarea type="text" name="securityProgramConcerns" class="form-control securityProgramConcerns cSuiteQuestionnaireTextArea" placeholder="Security Program Maturity" />Security Concerns</textarea>
	</div>
</div>

<div class="cSuiteDigitalMarketingSecretForm">
	<div class="cSuiteFormTitleTitle">
		Digital Marketing
	</div>
	<p>We would like to assess your Digital Marketing Leadership needs:</p>
	<div class="cSuiteQuestionnaireFieldTitle">
		Do you currently know your web traffic visitor numbers, metrics, and sources?
	</div>
	<div class="form-group cSuiteQuestionnaireFieldShell">
		<div>
			Yes <input checked value="1" type="radio" name="marketingMetricsYesNo" class="ssform-control marketingMetricsYesNo cSuiteQuestionnaireField"/> No <input type="radio" name="marketingMetricsYesNo" value="0" class="form-controls marketingMetricsYesNo cSuiteQuestionnaireField" />
		</div>
	</div>
	<div class="cSuiteQuestionnaireFieldTitle">
		What Digital Marketing Strategies are you currently employing?
	</div>
	<div class="form-group cSuiteQuestionnaireFieldShell">
		<div>
			<input checked class="form-controls cSuiteNDAFields" type="checkbox" id="marketingStrategies" name="marketingStrategies[]" value="PPC" /> Pay Per Click<br />
			<input class="form-controls cSuiteNDAFields" type="checkbox" id="marketingStrategies" name="marketingStrategies[]" value="Social" /> Social Media<br />
			<input checked class="form-controls cSuiteNDAFields" type="checkbox" id="marketingStrategies" name="marketingStrategies[]" value="Content Writing" /> Content Writing<br />
			<input checked class="form-controls cSuiteNDAFields" type="checkbox" id="marketingStrategies" name="marketingStrategies[]" value="Organic" /> Organic Keyword Targetting<br />
			<input class="form-controls cSuiteNDAFields" type="checkbox" id="marketingStrategies" name="marketingStrategies[]" value="Other Online Communities" /> Other Online Communities<br />
			<input class="form-controls cSuiteNDAFields" type="checkbox" id="marketingStrategies" name="marketingStrategies[]" value="Blogging" /> Blogging<br />
		</div>
	</div>
	<div class="cSuiteQuestionnaireFieldTitle">
		Do you have a living Digital Marketing Strategy. If Yes, please outline what that is.
	</div>
	<div class="form-group cSuiteQuestionnaireFieldShell">
		<textarea type="text" name="marketingStrategy" class="form-control marketingStrategy cSuiteQuestionnaireTextArea" placeholder="Digital Marketing Strategy" />We don't</textarea>
	</div>
</div>

<!--
<div>
	<a class="" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">
		An Online Mutual NDA Must be completed.
	</a>
</div>
<div>
	<a class="" data-toggle="collapse" href="#multiCollapseExample2" role="button" aria-expanded="false" aria-controls="multiCollapseExample2">
		The Fractional-CSuite Questionaire.
	</a>
</div>
<div class="collapse multi-collapse" id="multiCollapseExample1">
	<div class="card card-body">
		<p>
			The best way that our fractional executives can help is if they receive accurate and complete information about your business. We understand the information you may share can be highly sensitive and/or proprietary in nature - for our ears and eyes only. Also, we associate  an intrinsic value to the information and advice we may communicate to you.
		</p>
		<p>
			To protect each other's "crown jewels," we require a mutual non-disclosure agreement to be on-file prior to our response to an online questionnaire. Please take a moment to review the mutual non-disclosure agreement and complete the fields to agree and accept the terms. 
		</p>
	</div>
</div>
<div class="collapse multi-collapse" id="multiCollapseExample2">
	<div class="card card-body">
		<p>
			Our Questionaire is aimed to help us better understand the nature of your business and it's current state with regards to data security, legal, and digital marketing. Based on your answers, we'll be able to better gauge what leadership services you're currently in need of and how Fractional can elevate your business to the next level.
		</p>
	</div>
</div>
-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
<script type="text/javascript" src="<?php echo $tehAbsoluteURL; ?>js/jquery-1.11.1.js"></script>
<script type="text/javascript" src="<?php echo $tehAbsoluteURL; ?>js/jquery.validate.js"></script>

<script>
	$.validator.setDefaults( {
			submitHandler: function () {
			    event.preventDefault(); //prevent default action 
			    var post_url = $("#cSuiteQuestionnaireForm").attr("action"); //get form action url
			    var request_method = $("#cSuiteQuestionnaireForm").attr("method"); //get form GET/POST method
			    //var form_data = new FormData("#cSuiteQuestionnaireForm"); //Creates new FormData object
			    //var form_data = $("#cSuiteQuestionnaireForm").serialize();
			    var form_data = $("#cSuiteQuestionnaireForm");
			    $.ajax({
			        url : post_url,
			        type: request_method,
			        data : form_data.serialize(),
			        dataType: "json",
			        //contentType: false,
			        //cache: false,
			        //processData:false
			    }).done(function(response){ //
			        $("#server-results").html(form_data.serialize());
			    });
			}
		} );

		$( document ).ready( function () {
			$( "#cSuiteQuestionnaireForm" ).validate( {
				rules: {
					companyName: {
						required: true,
						minlength: 2,
						maxlength: 36
					},
					signedBy: {
						required: true,
						minlength: 2,
						maxlength: 36
					},
					jobTitle: {
						required: true,
						minlength: 2,
						maxlength: 36
					},
					IAgree: {
						required: true
					},
					elevatorPitch: {
						required: true,
						maxlength: 500
					},
					companyURL: {
						required: true
					},
					clientNumber: {
						required: true
					},
					projectedGrowth: {
						required: true
					},
					topCompetitors: {
						required: true,
						maxlength: 500
					},
					legalEntityYesNo: {
						required: true
					},
					formationDocs: {
						required: true
					},
					lookingForFinancing: {
						required: true
					},
					marketingMetricsYesNo: {
						required: true
					},
					marketingStrategies: {
						required: true
					},
					marketingStrategy: {
						required: true,
						maxlength: 500
					},
					usingAnAtorney: {
						required: true,
						maxlength: 500
					},
					formsChecklists: {
						required: true,
						maxlength: 500
					},
					engageVendors: {
						required: true,
						maxlength: 500
					},
					legalConcerns: {
						required: true,
						maxlength: 500
					},
					securityFrameworks: {
					},
					securityProgram: {
						required: true,
					},
					securityProgramMaturity: {
						required: true,
						maxlength: 500
					},
					securityThirdParty: {
						required: true,
					},
					securityProgramConcerns: {
						required: true,
						maxlength: 500
					}
				},
				messages: {
					companyName: {
						required: "Please enter your company name",
						minlength: "Company Name must be at least 2 characters",
						maxlength: "Company Name can be no more than 36 characters"
					},
					signedBy: {
						required: "Please enter your name",
						minlength: "Your Name must be at least 2 characters",
						maxlength: "Your Name can be no more than 36 characters"
					},
					jobTitle: {
						required: "Please enter your job title",
						minlength: "Job Title must be at least 2 characters",
						maxlength: "Job Title can be no more than 36 characters"
					},
					IAgree: {
						required: "You must agree to our Mutual NDA",
					},
					elevatorPitch: {
						required: "Field is required",
						maxlength: "Please limit your response to 500 characters"
					},
					companyURL: {
						required: "Field is required"
					},
					clientNumber: {
						required: "Field is required"
					},
					projectedGrowth: {
						required: "Field is required"
					},
					topCompetitors: {
						required: "Field is required",
						maxlength: "Please limit your response to 500 characters"
					},
					legalEntityYesNo: {
						required: "Field is required"
					},
					formationDocs: {
						required: "Field is required"
					},
					lookingForFinancing: {
						required: "Field is required"
					},
					marketingMetricsYesNo: {
						required: "Field is required"
					},
					marketingStrategies: {
						required: "Field is required"
					},
					marketingStrategy: {
						required: "Field is required",
						maxlength: "Please limit your response to 500 characters"
					},
					usingAnAtorney: {
						required: "Field is required",
						maxlength: "Please limit your response to 500 characters"
					},
					formsChecklists: {
						required: "Field is required",
						maxlength: "Please limit your response to 500 characters"
					},
					engageVendors: {
						required: "Field is required",
						maxlength: "Please limit your response to 500 characters"
					},
					legalConcerns: {
						required: "Field is required",
						maxlength: "Please limit your response to 500 characters"
					},
					securityFrameworks: {
					},
					securityProgram: {
						required: "Field is required",
					},
					securityProgramMaturity: {
						required: "Field is required",
						maxlength: "Please limit your response to 500 characters"
					},
					securityThirdParty: {
						required: "Field is required",
					},
					securityProgramConcerns: {
						required: "Field is required",
						maxlength: "Please limit your response to 500 characters"
					}
				},
				errorElement: "div",
				errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );

					// Add `has-feedback` class to the parent div.form-group
					// in order to add icons to inputs
					element.parents( ".cSuiteQuestionnaireFieldShell" ).addClass( "has-feedback" );

					if (( element.prop( "type" ) === "checkbox" ) || (element.prop( "type" ) === "radio" )){
						error.insertAfter( element.parent( "div" ) );
						//error.insertAfter( element.parent( ".burrrp" ) );
						//error.insertAfter( element );
						//error.insertAfter(".burrrp");
					} else {
						error.insertAfter( element );
					}

					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !element.next( "span" )[ 0 ] ) {
						$( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
					}
				},
				success: function ( label, element ) {
					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !$( element ).next( "span" )[ 0 ] ) {
						$( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".cSuiteQuestionnaireFieldShell" ).addClass( "has-error" ).removeClass( "has-success" );
					$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
				},
				unhighlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".cSuiteQuestionnaireFieldShell" ).addClass( "has-success" ).removeClass( "has-error" );
					$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
				}
			});
		} );
</script>

<?php
	require_once("tehPHP/kulaFooter.php")
?>