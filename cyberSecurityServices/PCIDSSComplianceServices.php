<?php
	$pageTitle = "CMB Networks | Arlington VA, PCI-DSS Compliance Services";
	$pageKeywords = "arlington PCI-DSS compliance services, arlington virginia PCI-DSS compliance services, arlington va PCI-DSS compliance services";
	$pageDesc = "With over 25 years of experience, explore our PCI-DSS Compliance Services or speak with one of our technical consultants, we'll find a solution that fits your needs and budget.";


	require_once("../tehPHP/kulaHeader.php");
?>

<style>
	.kulaContentWrapper{max-width: 1100px; margin: auto;}
	.kulaContentShadow{box-shadow: 3px 3px 40px #989898; min-height: 100px;}
	.kulaContentWindow{background: #FFFFFF; margin-top: 0; min-height: 100px; padding: 40px 40px; color: #333333;}
	h1 {color: #333333;}
	.kulaContentSubTitle{color: #52A595; font-size: 1.3em; margin-bottom: 20px;}
	.kulaContentParagraphImage{padding: 3px; border: 1px solid #989898; float: right; margin: 5px 10px;}
	.floatLeft{float: left !important;}


	/* DON't ADD THE ABOVE CSS */
	.kulaIconRightContainer{float: right; margin: 5px 10px; padding: 5px; text-align: center;}
	.kulaIconRightContainer > a{color: #333333; text-decoration: none; display: table-cell; padding: 20px 20px;}
	.kulaIconRightContainer > a:hover{background: #EFEFEF;}
	.kulaIconRightContainerTitle{text-align: center; font-weight: bold; padding: 20px 50px;}
	.kulaContentWindowNoListStyle{list-style:none;}
	.kulaContentWindowNoListStyle>li{padding: 10px 0;}
	.kulaContentWindowNoListStyle>li>div{font-weight: bold;}
</style>
<div class="mainIndexImage">
	<div class="container kulaHeaderPadding">
		<div class="row">
			<div class="col-md-12 kulaKeaderShell">
				<img src="<?php echo $tehAbsoluteURL; ?>layout/logos/CMBLogo.png" alt="Arlington Virginia Technical Consluting Logo" width="100%" />
			</div>
		</div>
	</div>
	<div class="kulaContentWrapper">
		<div class="kulaContentShadow">
			<div class="kulaContentWindow">
				<h1>PCI-DSS Compliance Services</h1>
				<div class="kulaContentSubTitle">
					DC Metro PCI-DSS Compliance Services
				</div>
				<p>
					If you are collecting credit card data from your customers it is your responsibility to protect that data, your customers are trusting that you do. If you are found to be out of compliance, you assume the risk and consequences from not being in compliance.
				</p>
				<p>
					PCI compliance is a serious security measure needed to ensure your customer credit card data is safe and secure. We have over <b>20 years</b> of experience protecting customer data from small online stores to enterprise level agencies such as the DC Metro Rail. Let our consultants find a solution to secure your customer data and ensure you're in compliance while storing their prescious credit card data.
				</p>
				<h1>PCI-DSS Security Standards</h1>
				<p>
					The Payment Card Industry Data Security Standard (PCI-DSS) was a security framework established by the major card brands such as Visa, American Express, Mastercard, and Discover.  Their intent was to create a standard set of security controls to help reduce fraud by organizations that handled, transmitted or stored credit card data on the internet. In 2004 the first PCI-DSS standard was released, version 1.0, that was the first set of controls for organizations that processed credit cards on the internet were mandated to comply with.
				</p>

				<h1> PCI-DSS Requirements </h1>
				<p>
					The PCI-DSS has major release ever 3 years, with sub-releases as necessary. The latest version, v3.2, was released April 2016.  There PCI-DSS standard is broken down into 12 major sections:
				</p>
				<ul class="kulaContentWindowNoListStyle">
					<li>
						<div>
							Build and Maintain a Secure Network
						</div>
						<ol>
							<li>
								Install and maintain a frewall confguration to protect cardholder data
							</li>
							<li>
								Do not use vendor-supplied defaults for system passwords and other security parameters
							</li>
						</ol>
					</li>
					<li>
						<div>
							Protect Cardholder Data 
						</div>
						<ol>
							<li>
								Protect stored cardholder data with encryption.
							</li>
							<li>
								Encrypt transmission of cardholder data across open, public networks
							</li>
						</ol>
					</li>
					<li>
						<div>
							Maintain a Vulnerability Management Program
						</div>
						<ol>
							<li>
								Use and regularly update anti-virus software or programs.
							</li>
							<li>
								Develop and maintain secure systems and applications.
							</li>
						</ol>
					</li>
					<li>
						<div>
							Implement Strong Access Control Measures
						</div>
						<ol>
							<li>
								Restrict access to cardholder data by business need to know.
							</li>
							<li>
								Assign a unique ID to each person with computer access.
							</li>
							<li>
								Restrict physical access to cardholder data.
							</li>
						</ol>
					</li>
					<li>
						<div>
							Regularly Monitor and Test Networks
						</div>
						<ol>
							<li>
								Track and monitor all access to network resources and cardholder data.
							</li>
							<li>
								Regularly test security systems and processes.
							</li>
						</ol>
					</li>
					<li>
						<div>
							Maintain an Information Security Policy
						</div>
						<ol>
							<li>
								Maintain a policy that addresses information security for all personnel.
							</li>
						</ol>
					</li>
				</ul>			
				<br style="clear: both;" />
			</div>
		</div>
	</div>
	<br /><br /><br />
</div>
<?php
	require_once("../tehPHP/kulaFooter.php")
?>