<?php
	$pageTitle = "Fractional C-Suite | Fractional Executive Services - CTO, CIO, CSO, CMO, COO, CFO, and Corporate Counsel";
	$pageKeywords = "";
	$pageDesc = "";


	require_once("tehPHP/kulaHeader.php");
?>
<script src="<?php echo $tehAbsoluteURL; ?>js/jquery.ez-bg-resize.js" type="text/javascript" charset="utf-8"></script>
<script>
	$(document).ready(function() {
	});
</script>
<div class="conferenceRoomBGDiv conferenceRoomIndexBG">
	<div class="cSuitMainLogoShell cSuiteWrapper">
		<img class="cSuiteMainLogo" src="<?php echo $tehAbsoluteURL; ?>layout/logos/fractionCSuiteLogo.png" alt="Fractional C-Suite Logo" title"We're in the Business of Leading Business" />
		<div class="cSuiteLogoText">
			We're in the Business of Leading Business
		</div>
	</div>
	<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
	<div class="cSuteMainTextBannerShell" style="background: #F8F8F8;">
		<h1 class="cSuiteMainTitle">
			Fractional Executive Leadership Services
		</h1>
		<h3 class="kulaLargerTitle" style="font-weight: normal; text-align: center;">
			Information Technology &amp; Security - Corporate Counsel - Digital Marketing
		</h3>
	</div>
</div>
	<style>
		.fractionalPopOutShadow{padding: 40px 10%;}
		.fractionalBoXPadding{padding: 120px 5%;}

		.conferenceRoomBGDiv{position: relative; top: -130px; margin-bottom: -180px; z-index: -1; width: 100%; min-height: 500px;padding-top: 5%;padding-bottom: 5%;background-size: cover;background-repeat: no-repeat;background-position: 25% 25%;}
		.conferenceRoomBGDiv:before, .conferenceRoomBGDiv:after{box-sizing: border-box;}

		.fractionalBGDiv{display: -webkit-flex;display: -ms-flexbox;display: flex;-webkit-flex-direction: column;-ms-flex-direction: column;flex-direction: column;-webkit-justify-content: middle;-ms-flex-pack: middle;justify-content: middle;-webkit-align-items: center;-ms-flex-align: center;align-items: left ;width: 100%; min-height: 500px;vertical-align: middle;padding-top: 5%;padding-bottom: 5%;background-size: cover;background-repeat: no-repeat;background-position: 25% 25%;position: relative; top: 50;}
		.fractionalBGDiv:before, .fractionalBGDiv:after{box-sizing: border-box;}

		.cityScapeIndexBG{background-image: url(./layout/backgrounds/skyscraperBG.jpg);}
		.conferenceRoomIndexBG{background-image: url(./layout/backgrounds/conferenceRoomBG.png); min-height: 1000px;}
		.cityScapeIndexBGShell{padding: 20px 10%; width: 100%;	}
		.fractionalBluePartialBG{background: #22566F; width: 80%; min-height: 200px; position: absolute; z-index: 1; top: 160px;}
		.fractionalBillboardWords{position: relative; z-index: 5; width: 100%; color: #FFFFFF;}

		.whatAreWeIconLinkShell:hover{text-decoration: none;}
		.whatAreWeIconShell{font-size: 1em; color: #898989; text-align: center; height: 500px;}
		.whatAreWeIconShell:hover{cursor: pointer; border-radius: 15px; border: 1px solid #CDCDCD; box-shadow: 10px 10px 40px #ABABAB;}
		.whatAreWeIcon{font-size: 6em;}
		.whatAreWeIconTitle{text-align: center; padding: 20px 5px;}
	</style>
<div class="fractionalBGDiv cityScapeIndexBG">
	<div class="cityScapeIndexBGShell">
		<div class="cSuiteWrapper">
			<!--
			<div class="cSuiteLetterTitleLetter" style="font-size: 9em;">
				  What is
			</div>
			<div class="cSuiteLetterTitleBottom">
				Fractional C-Suite?
			</div>
		-->
			<div class="row">
				<div class="col-sm-12 col-md-6 col-lg-3">
					<a href="#" class="whatAreWeIconLinkShell">
						<div class="columnShell whatAreWeIconShell">
							<span class="whatAreWeIcon icon-computer-1"></span>
							<div class="whatAreWeIconTitle">
								Chief Information<br/>Officer
							</div>
						</div>
					</a>
				</div>
				<div class="col-sm-12 col-md-6 col-lg-3">
					<a href="#" class="whatAreWeIconLinkShell">
						<div class="columnShell whatAreWeIconShell">
							<span class="whatAreWeIcon icon-data"></span>
							<div class="whatAreWeIconTitle">
								Chief Information<br />Security Officer
							</div>
						</div>
					</a>
				</div>
				<div class="col-sm-12 col-md-6 col-lg-3">
					<a href="#" class="whatAreWeIconLinkShell">
						<div class="columnShell whatAreWeIconShell">
							<span class="whatAreWeIcon icon-balance"></span>
							<div class="whatAreWeIconTitle">
								Corporate<br />Counsel
							</div>
						</div>
					</a>
				</div>
				<div class="col-sm-12 col-md-6 col-lg-3">
					<a href="#" class="whatAreWeIconLinkShell">
						<div class="columnShell whatAreWeIconShell">
							<span class="whatAreWeIcon icon-megaphone"></span>
							<div class="whatAreWeIconTitle">
								Digital<br />Marketing
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
<style>
	.lightBlueBG{background: #18A3C4; min-height: 200px; width: 100%;}
	.orangeBG{background: #FA3912; min-height: 200px; width: 100%;}
	.darkGrayBG{background: #686868; min-height: 200px; width: 100%;}
	.stdBoxShadow{box-shadow: 20px 20px 80px #ABABAB;}
	.columnShell{padding: 20px 15px; width: 100%;}
</style>
<div class="fractionalWhiteBG fractionalBoXPadding">
	<div class="row">
		<div class="row">
			<div class="col-sm-12 col-md-6 col-lg-6">
				<div class="lightBlueBG">
					<span class="icon-data" style="color: #FFFFFF"></span>
					<span class="icon-computer-1" style="color: #FFFFFF"></span>
					<span class="icon-megaphone" style="color: #FFFFFF"></span>
					<span class="icon-balance" style="color: #FFFFFF"></span>
				</div>
				<div class="orangeBG">
					<span class="icon-data" style="color: #FFFFFF"></span>
					<span class="icon-lock" style="color: #FFFFFF"></span>
					<span class="icon-computer-1" style="color: #FFFFFF"></span>
					<span class="icon-settings" style="color: #FFFFFF"></span>
					<span class="icon-data-1" style="color: #FFFFFF"></span>
					<span class="icon-hosting" style="color: #FFFFFF"></span>
					<span class="icon-balance" style="color: #FFFFFF"></span>
					<span class="icon-cloud" style="color: #FFFFFF"></span>
					<span class="icon-megaphone" style="color: #FFFFFF"></span>
					<span class="icon-teamwork" style="color: #FFFFFF"></span>
					<span class="icon-ascendant-bars-graphic" style="color: #FFFFFF"></span>
					<span class="icon-secure-shield" style="color: #FFFFFF"></span>
					<span class="icon-technology-2" style="color: #FFFFFF"></span>
					<span class="icon-technology-1" style="color: #FFFFFF"></span>
					<span class="icon-technology" style="color: #FFFFFF"></span>
					<span class="icon-computer" style="color: #FFFFFF"></span>
				</div>
				<div class="darkGrayBG">
				</div>
			</div>
			<div class="col-sm-12 col-md-6 col-lg-6">
				<div class="col-sm-12 col-md-12">
					<div class="columnShell stdBoxShadow">
						<div class="cSuiteLetterTitleLetter" style="font-size: 9em;">
							  W
						</div>
						<div class="cSuiteLetterTitle">
							<div class="cSuiteLetterTitleTop">
								What is
							</div>
							<div class="cSuiteLetterTitleBottom">
								Fractional C-Suite?
							</div>
						</div>
						<br /><br />
						<p class="cSuiteLandingPageParagraph">
						</p>
						<p class="cSuiteLandingPageParagraph">
							Fractional C-Suite offers businesses <b>Executive Leadership services, experience, and strategies</b>. With the proper knowledge and our expert leadership at your disposal your business will elevate you'll easily transition from the start-up mentality to a full blown enterprise with our enterprise-level  development business strategies and technologies.
						</p>
						<div class="row">
							<div class="col-sm-12 col-md-6">
								<ul class="cSuiteLeadershipServicesList">
									<li>Security Planning</li>
									<li>Infrastructure Solutions</li>
									<li>Cybersecurity Assessment &amp; Training</li>
									<li>Process Implementation</li>
									<li>Cloud Solutions</li>
									<li>Technology Business Solutions Strategies</li>
									<li>Governance Risk &amp; Compliance</li>
									<li>Strategic Roadmap</li>
									<li>Business Strategy</li>
									<li>PCI Compliance</li>
									<li>ITIL Process Alignment</li>
									<li>Security Training Program</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--
<div class="fractionalBGDiv cityScapeIndexBG">
	<div class="fractionalBluePartialBG"></div>
	<div class="fractionalBillboardWords">
		<div class="row">
			<div class="col-sm-12 col-md-6 col-lg-6">
				words
			</div>
			<div class="col-sm-12 col-md-6 col-lg-6">
				words again
			</div>
		</div>
	</div>
</div>
<div class="cSuteMainTextBannerShell" style="background: #F8F8F8;">
	<h1 class="cSuiteMainTitle">
		Fractional Executive Leadership Services
	</h1>
	<h3 class="kulaLargerTitle" style="font-weight: normal; text-align: center;">
		Information Technology &amp; Security - Corporate Counsel - Digital Marketing
	</h3>
</div>
-->
<div class="cSuiteWrapper">
	<div class="cSuiteLetterTitleShell">
		<div class="cSuiteLetterTitleLetter">
			E
		</div>
		<div class="cSuiteLetterTitle">
			<div class="cSuiteLetterTitleTop">
				Executive Leadership
			</div>
			<div class="cSuiteLetterTitleBottom">
				Fractional Pricing
			</div>
		</div>
	</div>

	<br style="clear: both;" />
		<div class="cSuiteBlockQuoteShell cSuiteModifiedBlockQuote">
			<br /><br />
			<p class="cSuiteFractionalPriceExample">
				CIO/CTO Salary - $204,000/year
			</p>
			<p class="cSuiteFractionalPriceExampleBlue">
				Fractional Packages Starting at $25,000/year
			</p>
			<p class="cSuiteFractionalPriceSource">
				Salary represents hirings in the Miami Metro Area
			</p>
			<div class="cSuiteGrowButtonShell">
				<a href="./fractionalCTOServices/index.php" class="kulaLargerTitle cSuiteGrowButton" style="font-weight: normal; text-align: center; font-size: .8em;">
					Learn About Our Fractional CTO/CIO Services
				</a>
			</div>
			<br />
		</div>
		<div class="cSuiteBlockQuoteShell cSuiteModifiedBlockQuote">
			<br /><br />
			<p class="cSuiteFractionalPriceExample">
				CSO Salary - $204,000/year
			</p>
			<p class="cSuiteFractionalPriceExampleBlue">
				Fractional Packages Starting at $25,000/year
			</p>
			<p class="cSuiteFractionalPriceSource">
				Salary represents hirings in the Miami Metro Area
			</p>
			<div class="cSuiteGrowButtonShell">
				<a href="./fractionalCTOServices/index.php" class="kulaLargerTitle cSuiteGrowButton" style="font-weight: normal; text-align: center; font-size: .8em;">
					View Our Fractional CSO Services
				</a>
			</div>
			<br />
		</div>
	</div>
	<br style="clear: both;" />
</div>
	<div class="snowBirdBGDiv snowBirdIndexBG">
		<div class="cSuiteWrapper">
			<div class="cSuiteLandingPageParagraphShell">
				<div class="cSuiteLetterTitleShell" style="font-size: .5em;">
					<div class="cSuiteLetterTitleLetter" style="color: #FFFFFF; z-index: 1;">
						C
					</div>
					<div class="cSuiteLetterTitle">
						<div class="cSuiteLetterTitleTop">
							Corporate Counsel
						</div>
						<div class="cSuiteLetterTitleBottom">
							Legal Leadership
						</div>
					</div>
				</div>
				<div class="cSuiteLandingPageParagraphTitle">
					Our Fractional <b>Corporate Counsel</b> Role.
				</div>
				<p>
					We understand that you're not a lawyer and generally, hiring lawyers is rather pricey so most start-ups neglect legal representation until it's too late. This leaves your new company in a compromising situation that's typically even more expensive to climb out of than if you'd hired representation in the beginning. Fractional-CSuite builds efficient legal departsments in the beginning. Keeping your contract negotiations and sales teams flowing, optimizing your company's revenue stream. With 20+ years of experience in the IT world our talents are many from simple contract management to litigation support and risk assessment profiling. 
				</p>
				<br />
				<div class="row">
					<div class="col-sm-12 col-md-6">
						<ul class="cSuiteLeadershipServicesList">
							<li>Build an efficient legal department</li>
							<li>Contract Law &amp; Negotiations</li>
							<li>Privacy and Data Law</li>
						</ul>
					</div>
					<div class="col-sm-12 col-md-6">
						<ul class="cSuiteLeadershipServicesList">
							<li>Terminations</li>
							<li>Litigation Support</li>
							<li>Risk Assessment and Profiles</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="cSuiteBlockQuoteShell cSuiteModifiedBlockQuote" style="box-shadow: none;">
				<br /><br />
				<p class="cSuiteFractionalPriceExample">
					Average Corporate General Counsel Salary- $209,000/year
				</p>
				<p class="cSuiteFractionalPriceExampleBlue">
					Fractional Packages Starting at $25,000/year
				</p>
				<p class="cSuiteFractionalPriceSource">
					Salary Data collected as a National Average
				</p>
				<div class="cSuiteGrowButtonShell">
					<a href="./fractionalCorporateCounsel/index.php" class="kulaLargerTitle cSuiteGrowButton" style="font-weight: normal; text-align: center; font-size: .8em;">
						View Our Fractional Corporate Counsel Services
					</a>
				</div>
				<br /><br />
			</div>
		</div>
</div>
	<style>
		.snowBirdIndexBG{background-image: url(./layout/backgrounds/courtHouse.jpg);}
		.snowBirdBGDiv{display: -webkit-flex;display: -ms-flexbox;display: flex;-webkit-flex-direction: column;-ms-flex-direction: column;flex-direction: column;-webkit-justify-content: middle;-ms-flex-pack: middle;justify-content: middle;-webkit-align-items: center;-ms-flex-align: center;align-items: center;width: 100%;min-height: 400px;vertical-align: middle;padding-top: 5%;padding-bottom: 5%;background-size: cover;background-repeat: no-repeat;background-position: 75% 75%;position: relative;}
		.snowBirdBGDiv:before, .snowBirdBGDiv:after{box-sizing: border-box;}
	</style>
<div class="cSuiteWrapper">
	<div class="cSuiteLandingPageParagraphShell">
		<div class="cSuiteLetterTitleShell" style="font-size: .5em;">
			<div class="cSuiteLetterTitleLetter">
				M
			</div>
			<div class="cSuiteLetterTitle">
				<div class="cSuiteLetterTitleTop">
					Digital Marketing
				</div>
				<div class="cSuiteLetterTitleBottom">
					Executive Marketing Leadership
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12 col-md-6">
				
			</div>
			<div class="col-sm-12 col-md-6">
				<ul class="cSuiteLeadershipServicesList">
					<li>Webpresence Assessment</li>
					<li>Content Development &amp; Delivery</li>
					<li>Content Coaching &amp; Training</li>
				</ul>
			</div>
		</div>
		<div class="cSuiteLandingPageParagraphTitle">
			Fractional's <b>Chief (Digital) Marketing Officer</b> Services.
		</div>
		<p>
			Let's face it. Marketing these days is about 90% digital. With our Digital Marketing Strategies we've landed thousands of keywords within the top page's of Google's Page Results resulting in huge increases in traffic and lead generations. Our leadership team will develop a measurable digital marketing strategy guaranteed to improve lead generation. 
		</p>
					<br />
			<div class="row">
				<div class="col-sm-12 col-md-6">
					<ul class="cSuiteLeadershipServicesList">
						<li>Digital Marketing Analytics</li>
						<li>Digital Marketing Strategies</li>
						<li>Digital Marketing Tools</li>
					</ul>
				</div>
				<div class="col-sm-12 col-md-6">
					<ul class="cSuiteLeadershipServicesList">
						<li>Webpresence Assessment</li>
						<li>Content Development &amp; Delivery</li>
						<li>Content Coaching &amp; Training</li>
					</ul>
				</div>
		<div class="cSuiteBlockQuoteShell cSuiteModifiedBlockQuote">
			<br /><br />
			<p class="cSuiteFractionalPriceExample">
				Average Chief Marketing Officer Salary - $189,000/year
			</p>
			<p class="cSuiteFractionalPriceExampleBlue">
				Fractional Packages Starting at $20,000/year
			</p>
			<p class="cSuiteFractionalPriceSource">
				Salary Data collected as a National Average
			</p>
			<div class="cSuiteGrowButtonShell">
				<a href="./fractionalDigitalMarketing/index.php" class="kulaLargerTitle cSuiteGrowButton" style="font-weight: normal; text-align: center; font-size: .8em;">
					View Our Fractional Chief Digital Marketing Services
				</a>
			</div>
			<br /><br />
		</div>
	</div>
</div>
<br style="clear: both;" />
<br /><br /><br /><br /><br /><br />


	<div class="container cSuiteHeaderPadding">
		<h1 class="cSuiteMainTitle">
			Let's Discuss Your Leadership Needs
		</h1>
		<div class="cSuiteGrowButtonShell">
			<a href="./gettingStartedWithFractional.php" class="kulaLargerTitle cSuiteGrowButton" style="font-weight: normal; text-align: center;">
				Start Here
			</a>
		</div>
	</div>
</div>
<?php
	require_once("tehPHP/kulaFooter.php")
?>