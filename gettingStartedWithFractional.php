<?php
	$pageTitle = "Fractional C-Suite | Getting Started With Fractional Executive Services";
	$pageKeywords = "fractional onboarding process, fractional csuite onboarding process, how do I get started with fractional, how do I get started with fractional csuite, how do I get started with fractional-csuite";
	$pageDesc = "A look at Fractional's onboarding process and how to get started with Fractional.";


	require_once("tehPHP/kulaHeader.php");
?>
<script src="./js/jquery.ez-bg-resize.js" type="text/javascript" charset="utf-8"></script>
<script>
	$(document).ready(function() {
		
	});
</script>
<style>
	.cSuiteStartInfoGraphicShell{}
	.cSuiteInfoGraphicCircleShell{height: 60px; width: 60px; float: left; margin: 10px 10px;}

	.cSuiteInfoGraphicCircle {
		position: relative;
		display: inline-block;
		width: 100%;
		height: 0;
		line-height: 0;
		padding: 50% 0;
		border-radius: 50%;
  
	  /* Just making it pretty */
	  @shadow: rgba(0, 0, 0, .1);
	  @shadow-length: 4px;
	  -webkit-box-shadow: 0 @shadow-length 0 0 @shadow;
	          box-shadow: 0 @shadow-length 0 0 @shadow;
	  text-shadow: 0 @shadow-length 0 @shadow;
	  background: #18A3C4;
	  color: white;
	  font-family: Helvetica, Arial Black, sans;
	  font-size: 30px;
	  text-align: center;
	}
	.cSuiteInfoGraphicTextShell{font-size: 1.4em; padding: 10px 0 0 5px; font-weight: bold;}
	.cSuiteInfoGraphicOrange{background: #FA3912;}
	.cSuiteInfoGraphicTextShell>span{font-size:.8em; font-weight: normal;}
	.cSuitStartInfoGraphicImageShell{float: left;}
	
	.cSuiteInfoGraphicOne{position: relative; left: -80px;}
	.cSuiteInfoGraphicFour{position: relative; left: -80px;}
	.cSuiteLightBlueBG{background: #A9E1EE ;}
	.cSuiteGrowButtonShellPreText{color: #18A3C4; font-size: 2em; text-align: center; line-height: .5; font-style: italic; text-transform: none;}
</style>
<div class="mainIndexImage">
	<div class="container cSuiteHeaderPadding">
		<div class="row">
			<div class="col-md-12 kulaKeaderShell">
				<br /><br />
				<div class="cSuitMainLogoShell">
					<img class="cSuiteMainLogo" src="<?php echo $tehAbsoluteURL; ?>layout/logos/fractionCSuiteLogo.png" alt="" />
					<div class="cSuiteLogoText">
						Getting Started With Fractional C-Suite
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<br /><br /><br /><br /><br />
<div class="cSuiteWrapper cSuiteLightBlueBG">
	<h1 class="cSuiteMainTitle" style="padding: 40px 0 60px 0; color: #22566F;">
		The Onboarding Process
	</h1>
	<table align="center" style="max-width: 900px; color: #22566F;">
		<tr>
			<td style="padding-right: 25px;">
				<div class="cSuitStartInfoGraphicImageShell">
					<img class="cSuitStartInfoGraphicImage" alt="Getting Started Fractional C-Suite" title="An Overview of the Leadership Questionaire and NDA" src="<?php echo $tehAbsoluteURL; ?>layout/images/gettingStartedTable.png" />
				</div>
			</td>
			<td style="padding: 10px 25px;">
				<div class="cSuiteStartInfoGraphicShell">
					<div class="cSuiteInfoGraphicOne">
						<div class="cSuiteInfoGraphicCircleShell">
							<span class="cSuiteInfoGraphicCircle">
						        1
							</span>
						</div>
						<div class="cSuiteInfoGraphicTextShell">
							Complete our <a href="<?php echo $tehAbsoluteURL; ?>cSuiteQuestionnaire.php">Mutual NDA &amp; Leadership Questionaire</a>. - <span>Simple and straightforward</span>
						</div>
						<br style="clear: both;" />
					</div>
					<div class="cSuiteInfoGraphicTwo">
						<div class="cSuiteInfoGraphicCircleShell">
							<span class="cSuiteInfoGraphicCircle">
						        2
							</span>
						</div>
						<div class="cSuiteInfoGraphicTextShell">
							We will contact you! (3-5 Business Days) - <span>It takes us some time to understand your business and its current state. Follow up questions may be in order to finalize the leadership assessment.</span>
						</div>
						<br style="clear: both;" />
					</div>
					<div>
						<div class="cSuiteInfoGraphicCircleShell">
							<span class="cSuiteInfoGraphicCircle cSuiteInfoGraphicOrange">
						        3
							</span>
						</div>
						<div class="cSuiteInfoGraphicTextShell">
							Our first deliverable - <span>We will provide an initial leadership assessment. Including a strategy, timeline, and a retainer needed to move forward.</span>
						</div>
						<br style="clear: both;" />
					</div>
					<div class="cSuiteInfoGraphicFour">
						<div class="cSuiteInfoGraphicCircleShell">
							<span class="cSuiteInfoGraphicCircle">
						        4
							</span>
						</div>
						<div class="cSuiteInfoGraphicTextShell">
							With an Agreement Reached - <span>We'll start working right away (Or as agreed upon).</span>
						</div>
						<br style="clear: both;" />
					</div>
				</div>
			</td>
		</tr>
	</table>
	<br /><br /><br />
</div>
<br /><br /><br /><br /><br />
<!--
<div>
	<a class="" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">
		An Online Mutual NDA Must be completed.
	</a>
</div>
<div>
	<a class="" data-toggle="collapse" href="#multiCollapseExample2" role="button" aria-expanded="false" aria-controls="multiCollapseExample2">
		The Fractional-CSuite Questionaire.
	</a>
</div>
<div class="collapse multi-collapse" id="multiCollapseExample1">
	<div class="card card-body">
		<p>
			The best way that our fractional executives can help is if they receive accurate and complete information about your business. We understand the information you may share can be highly sensitive and/or proprietary in nature - for our ears and eyes only. Also, we associate  an intrinsic value to the information and advice we may communicate to you.
		</p>
		<p>
			To protect each other's "crown jewels," we require a mutual non-disclosure agreement to be on-file prior to our response to an online questionnaire. Please take a moment to review the mutual non-disclosure agreement and complete the fields to agree and accept the terms. 
		</p>
	</div>
</div>
<div class="collapse multi-collapse" id="multiCollapseExample2">
	<div class="card card-body">
		<p>
			Our Questionaire is aimed to help us better understand the nature of your business and it's current state with regards to data security, legal, and digital marketing. Based on your answers, we'll be able to better gauge what leadership services you're currently in need of and how Fractional can elevate your business to the next level.
		</p>
	</div>
</div>
-->


<div class="container cSuiteHeaderPadding">
	<h1 class="cSuiteMainTitle">
		Keep the Onboarding Process moving.
	</h1>
	<div class="cSuiteGrowButtonShellPreText">
		Take me to the
	</div>
	<div class="cSuiteGrowButtonShell">
		<center>
			<a href="<?php echo $tehAbsoluteURL; ?>cSuiteQuestionnaire.php" class="kulaLargerTitle cSuiteGrowButton" style="display: table-cell;">
				<div>
					Questionaire &amp; NDA
				</div>
			</a>
		</center>
	</div>
</div>
<?php
	require_once("tehPHP/kulaFooter.php")
?>