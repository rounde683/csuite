<style>
	.kulaFooterShell{width: 100%; background: #18A3C4; color: #FFFFFF;}
	.kulaSilhouetteShell{margin-top: 50px;height: 93px; background: URL(<?php echo $tehAbsoluteURL; ?>layout/backgrounds/citySilhouette.png);}
	.kulaFooterIconShell>p{font-size: 2em;}
	.kulaFooterTitle{color: #FFFFFF; font-size: 1.3em; font-weight: bold;}
	.kulaFooterList{list-style: none; margin: 0;}
	.kulaFooterList>li>a{color: #FFFFFF;}
	.kulaFooterList>li>a:hover{text-decoration: underline;}
	.cSuiteFooterLogo{width: 100%; max-width: 600px; margin: 30px 0;}
</style>
		<div class="kulaSilhouetteShell">
		</div>
		<div class="kulaFooterShell">
			<div class="kulaWrapper">
				<div class="container cSuiteHeaderPadding">
					<div class="row">
						<div class="col-md-12 kulaFooterIconShell">
							<p style="text-align: center;">
								"The essence of strategy is choosing what not to do." <em>- Michael Porter</em>
							</p>
							<center>
								<img class="cSuiteFooterLogo" src="<?php echo $tehAbsoluteURL; ?>layout/logos/fractionalCSuiteLogoWhite.png" alt="fractional CSuite Logo" width="100%" />
							</center>

							<br /><br />
						</div>
					</div>
					<div class="row">
						<div class="col-md-12" style="text-align: center">
							<ul class="kulaFooterList">
								<li>
									<i class="fa fa-phone"></i> (888) 310-4785
								</li>
								<li>
									<i class="fa fa-envelope-open"></i> info@fractional-csuite.com
								</li>
								<li>
									<i class="fa fa-clock-o"></i> <span>Mon - Fri 9am - 5pm</span>
								</li>
								<li>
									<i class="fa fa-map-marker"></i> <span>315 S 4th St. #200<sup>st</sup> Ft. Lauderdale, FL</span>
								</li>
							</ul>
						</div>
						<div class="col-md-12" style="text-align: center">
							Copyright &copy; <?php echo date("Y");?> fractional C-Suite LLC. All Rights are Reserved.
						</div>
					</div>
					<!--
					<div class="row">
						<div class="col-md-4 kulaFooterIconShell">
							<img src="<?php echo $tehAbsoluteURL; ?>layout/logos/fractionalCSuiteLogoWhite.png" slt="Arlington Virginia Technical Consluting Logo" width="100%" />
							<br /><br />
							<p>
								"The essence of strategy is choosing what not to do."
							</p>
							<p>
								<em>- Michael Porter</em>
							</p>
						</div>
						<div class="col-md-4 kulaFooterIconShell">
							<div class="kulaFooterTitle">
								Our Services
							</div>
							<ul class="kulaFooterList">
								<li><a href="<?php echo $tehAbsoluteURL; ?>cyberSecurityServices/">PCI-DSS Compliance Services</a></li>
								<li><a href="#">Risk Management</a></li>
								<li><a href="#">IT Infrastructure</a></li>
								<li><a href="#">Cloud Infrastructure</a></li>
								<li><a href="<?php echo $tehAbsoluteURL; ?>businessIntelligenceServices/">Business Intelligence</a></li>
							</ul>
						</div>
						<div class="col-md-4 kulaFooterIconShell">
							<div class="kulaFooterTitle">
								Contact Us
							</div>
							<ul class="kulaFooterList">
								<li>
									<i class="fa fa-phone"></i> (801) 310-4785
								</li>
								<li>
									<i class="fa fa-envelope-open"></i> info@fractionalc-suite.com
								</li>
								<li>
									<i class="fa fa-clock-o"></i> <span>Mon - Fri 9am - 5pm</span>
								</li>
								<li>
									<i class="fa fa-map-marker"></i> <span>315 S 4th St. #200<sup>st</sup> Ft. Lauderdale, FL</span>
								</li>
							</ul>
						</div>
					</div>
					-->
				</div>
			</div>
			<br />
		</div>
	</body>
</html>