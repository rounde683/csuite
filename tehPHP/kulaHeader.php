<?php $tehAbsoluteURL = "http://localhost/csuiteNew/"; ?>
<?php $tehAbsoluteURL = "https://fractional-csuite.com/"; ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"	xml:lang="en">
	<head>
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

		<script src="<?php echo $tehAbsoluteURL; ?>js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="<?php echo $tehAbsoluteURL; ?>js/jquery-3.2.1.min.js" type="text/javascript" charset="utf-8"></script>
		<!--
			<script src="<?php echo $tehAbsoluteURL; ?>js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
			<script src="<?php echo $tehAbsoluteURL; ?>js/jquery-3.2.1.min.js" type="text/javascript" charset="utf-8"></script>
			<script src="http://code.jquery.com/jquery-latest.min.js"></script>
			<link rel="stylesheet" href="<?php echo $tehAbsoluteURL; ?>css/bootstrap.css">
		-->


		<link rel="stylesheet" type="text/css" href="<?php echo $tehAbsoluteURL; ?>css/default.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $tehAbsoluteURL; ?>css/font-awesome_min.css" />
		<link rel="stylesheet" href="<?php echo $tehAbsoluteURL; ?>css/bootstrap.css">

		<link href="<?php echo $tehAbsoluteURL; ?>css/mega_menu.css" rel="stylesheet">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<script src="<?php echo $tehAbsoluteURL; ?>js/mega_menu.min.js" type="text/javascript" charset="utf-8"></script>

		<link rel="SHORTCUT ICON" href="<?php echo $tehAbsoluteURL; ?>favicon.ico" />
		<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
		<link rel="manifest" href="/manifest.json">
		<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="theme-color" content="#ffffff">

		<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>

		<link rel="manifest" href="<?php echo $tehAbsoluteURL; ?>manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?php echo $tehAbsoluteURL; ?>ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		
		<title><?php echo $pageTitle; ?></title>
		<meta http-equiv="Content-Type"	content="text/html; charset=iso-8859-1" />
		<meta name='keywords' content="<?php echo $pageKeywords; ?>" />
		<meta name='description' content="<?php echo $pageDesc; ?>" />

		
		<style>
			.kulaKeaderShell{padding: 30px 10px;}
			.mainMenuShell{background: #22566F ;}
			.kulaContactSubtitle{color: #77C21B; text-align: center;}
			.kulaHeaderUpowrkText{padding: 20px 10px; font-size: .8em;}
			.kulaHeaderUpowrkTextShell{padding-top: 10px; width: 100%; text-align: center;}
		</style>
		<script src="<?php echo $tehAbsoluteURL; ?>js/jquery.ez-bg-resize.js" type="text/javascript" charset="utf-8"></script>
		<script>
			$(document).ready(function() {
				// $(".mainIndexImage").ezBgResize({
				// 	img     : "<?php echo $tehAbsoluteURL; ?>layout/backgrounds/internetConsultantBGW.jpg", // Relative path example.  You could also use an absolute url (http://...).
				// 	opacity : 1, // Opacity. 1 = 100%.  This is optional.
				// 	center  : true // Boolean (true or false). This is optional. Default is true.
				// });
			});
		</script>
	</head>
	<body>
		<div class="maisnMenuShell">
			<?php
				//if( (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1") || (dirname($_SERVER['PHP_SELF']) == "/"))
				//if (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1")
				if(dirname($_SERVER['PHP_SELF']) == "/")
				{
					require_once("./tehPHP/mainMenu.php");
				}
				else
				{
					require_once("../tehPHP/mainMenu.php");
				}
			?>
		</div>
<!--
		<div class="container kulaHeaderPadding">
			<div class="row">
				<div class="col-md-9 kulaKeaderShell">
					<img src="<?php echo $tehAbsoluteURL; ?>layout/logos/CMBLogo.png" slt="Arlington Virginia Technical Consluting Logo" width="80%" />
				</div>
				<div class="col-md-3 kulaKeaderShell">
					<div class="kulaContactSubtitle">
						<strong>
							Hire Us as your IT Department
						</strong>
					</div>
					<div class="kulaEmailTitleButton">
						<div class="kulaGrowButtonShell">
							<a href="#" class="growButtonContact">
								Contact Us
							</a>
						</div>
					</div>
					<div class="kulaHeaderUpowrkTextShell">
						<div class="col-sm-6">
							<img src="<?php echo $tehAbsoluteURL; ?>layout/logos/upWorkLogo.png" width="40" />
						</div>
						<div class="col-sm-6">
							<span class="kulaHeaderUpowrkText">
								100% Job Success Rate
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		-->