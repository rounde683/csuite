
<script src="./js/mega_menu.min.js" type="text/javascript" charset="utf-8"></script>
 <script>
        jQuery(document).ready(function ($) {
            $('#menu-1').megaMenu({
                // DESKTOP MODE SETTINGS
                logo_align          : 'left',    // align the logo left or right. options (left) or (right)
                links_align         : 'left',    // align the links left or right. options (left) or (right)
                socialBar_align     : 'left',    // align the socialBar left or right. options (left) or (right)
                searchBar_align     : 'right',   // align the search bar left or right. options (left) or (right)
                trigger             : 'hover',   // show drop down using click or hover. options (hover) or (click)
                effect              : 'fade',    // drop down effects. options (fade), (scale), (expand-top), (expand-bottom), (expand-left), (expand-right)
                effect_speed        : 400,       // drop down show speed in milliseconds
                sibling             : true,      // hide the others showing drop downs if this option true. this option works on if the trigger option is "click". options (true) or (false)
                outside_click_close : true,      // hide the showing drop downs when user click outside the menu. this option works if the trigger option is "click". options (true) or (false)
                top_fixed           : false,     // fixed the menu top of the screen. options (true) or (false)
                sticky_header       : false,     // menu fixed on top when scroll down down. options (true) or (false)
                sticky_header_height: 200,       // sticky header height top of the screen. activate sticky header when meet the height. option change the height in px value.
                menu_position       : 'horizontal',    // change the menu position. options (horizontal), (vertical-left) or (vertical-right)
                full_width          : false,            // make menu full width. options (true) or (false)
                // MOBILE MODE SETTINGS
                mobile_settings     : {
                    collapse            : true, // collapse the menu on click. options (true) or (false)
                    sibling             : true,  // hide the others showing drop downs when click on current drop down. options (true) or (false)
                    scrollBar           : true,  // enable the scroll bar. options (true) or (false)
                    scrollBar_height    : 400,   // scroll bar height in px value. this option works if the scrollBar option true.
                    top_fixed           : false, // fixed menu top of the screen. options (true) or (false)
                    sticky_header       : false, // menu fixed on top when scroll down down. options (true) or (false)
                    sticky_header_height: 200    // sticky header height top of the screen. activate sticky header when meet the height. option change the height in px value.
                }
            });
        });
    </script>
    <style>
        .kulaMenuIconShell{text-align: center;}
        .kulaMenuIconShell>a{padding: 5px 50px; text-align: center !important; display: block; height: 130px;}
        .kulaMenuIconShell>a:hover{background: #CDCDCD;}
        .kulaMenuIconShell>a>img{margin: 5px auto !important; float: none !important}
        .kulaMenuIconShell>a>div{font-weight: bold; text-align: center;}
    </style>
<nav id="menu-1" class="mega-menu" data-color="">
    <section class="menu-list-items">
        <ul class="menu-logo">
            <li>
                <a href="http://fractional-csuite.com/contactUs.php"> <i style="color:#A9E1EE;" class="fa fa-phone"></i> (888) 310.478 </a>
            </li>

        </ul>
        <ul class="menu-links">
            <li><a href="<?php echo $tehAbsoluteURL; ?>"> <i class="fa fa-home"></i> Home</a></li>
            
            <li>
                <a href="javascript:void(0)" title="What is Fractional?">
                    What's Fractional?
                </a>
                <!--
                <a href="javascript:void(0)" title="Chief Information Technology &amp; Security Services">
                    <i class="fa fa-server"></i> CTO/CSO <i class="fa fa-angle-down fa-indicator"></i>
                </a>

                <!-- drop down full width -->
                <div class="drop-down grid-col-12">
                    <!--grid row-->
                    <div class="grid-row">
                        <!--grid column 2-->
                        <div class="grid-col-2">
                            <div class="kulaMenuIconShell">
                                <!--
                                <a class="" href="<?php echo $tehAbsoluteURL; ?>serverAdministrationServices/">
                                    <img  style="margin: 5px auto;" src="<?php echo $tehAbsoluteURL; ?>/layout/icons/grayInfrastructure.png" alt="DC Metro Area Server Administration Services" width="40"/>
                                    <div>
                                        DC Metro Area<br />Server Administration Services
                                    </div>
                                </a>
                            -->
                            </div>
                        </div>
                        <div class="grid-col-2">
                            <div class="kulaMenuIconShell">
                                <a class="" href="<?php echo $tehAbsoluteURL; ?>serverAdministrationServices/linuxServerAdministration.php">
                                    <img  style="margin: 5px auto;" src="<?php echo $tehAbsoluteURL; ?>/layout/icons/linuxIcon.png" alt="DC Metro Area Server Administration Services" width="40"/>
                                    <div>
                                        DC Metro Area<br />Linux Server Administration Services
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="grid-col-2">
                            <div class="kulaMenuIconShell">
                                <a class="" href="<?php echo $tehAbsoluteURL; ?>serverAdministrationServices/windowsServerAdministration.php">
                                    <img  style="margin: 5px auto;" src="<?php echo $tehAbsoluteURL; ?>/layout/icons/windowsIcon.png" alt="DC Metro Area Server Administration Services" width="40"/>
                                    <div>
                                        DC Metro Area<br />Windows Server Administration Services
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="grid-col-2">
                            <div class="kulaMenuIconShell">
                                <a class="" href="<?php echo $tehAbsoluteURL; ?>businessIntelligenceServices/">
                                    <img  style="margin: 5px auto;" src="<?php echo $tehAbsoluteURL; ?>/layout/icons/businessIntelligenceIcon.png" alt="DC Metro Area Business Intelligence Services" width="40"/>
                                    <div>
                                        DC Metro Area<br />Business Intelligence
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="grid-col-2">
                            <div class="kulaMenuIconShell">
                                <a class="" href="<?php echo $tehAbsoluteURL; ?>cyberSecurityServices/PCIDSSComplianceServices.php">
                                    <img  style="margin: 5px auto;" src="<?php echo $tehAbsoluteURL; ?>/layout/icons/PCIDSSComplianceIcon.png" alt="DC Metro Area PCI-DSS Compliance Services Services" width="40"/>
                                    <div>
                                        DC Metro Area<br />PCI-DSS Compliance Services
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </li>
            <li>
                <a href="javascript:void(0)" title="What We Do">
                    What We Do
                </a>
                <!--
                <a href="javascript:void(0)" title="Corporate Counsel Services">
                    <i class="fa fa-briefcase"></i> Corporate Counsel <i class="fa fa-angle-down fa-indicator"></i>
                </a>

                <!-- drop down full width -->
                <div class="drop-down grid-col-12">
                    <h3>
                        Corporate Counsel Leadership Services
                    </h3>
                </div>

            </li>
            <li><a href="javascript:void(0)" title="Chief (Digital) Marketing Officer Services">
                 <i class="fa fa-line-chart"></i> Marketing <i class="fa fa-angle-down fa-indicator"></i></a>

                <!-- drop down full width -->
                <div class="drop-down grid-col-12">
                    <h3>
                        Digital Marketing Leadership Services
                    </h3>
                    <div class="row">
                        <div class="col-sm-12 col-md-4 col-lg-4">
                            Analytics
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4">
                            Strategy
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4">
                            Tools
                        </div>
                    </div>
                    <h3>
                        Full Suite of Digital Marketing Services
                    </h3>
                </div>

            </li>
            <!--
            <li><a href="javascript:void(0)">Grid <i class="fa fa-angle-down fa-indicator"></i></a>

                <div class="drop-down grid-col-12 grid-demo offset-3-vertical">
                    <div class="grid-row">
                        <div class="grid-col-1"><span>grid-col-1</span></div>
                        <div class="grid-col-11"><span>grid-col-11</span></div>
                    </div>
                    <div class="grid-row">
                        <div class="grid-col-10"><span>grid-col-10</span></div>
                        <div class="grid-col-2"><span>grid-col-2</span></div>
                    </div>
                    <div class="grid-row">
                        <div class="grid-col-3"><span>grid-col-3</span></div>
                        <div class="grid-col-9"><span>grid-col-9</span></div>
                    </div>
                    <div class="grid-row">
                        <div class="grid-col-8"><span>grid-col-8</span></div>
                        <div class="grid-col-4"><span>grid-col-4</span></div>
                    </div>
                    <div class="grid-row">
                        <div class="grid-col-5"><span>grid-col-5</span></div>
                        <div class="grid-col-7"><span>grid-col-7</span></div>
                    </div>
                    <div class="grid-row">
                        <div class="grid-col-1"><span>grid-col-1</span></div>
                        <div class="grid-col-1"><span>grid-col-1</span></div>
                        <div class="grid-col-1"><span>grid-col-1</span></div>
                        <div class="grid-col-1"><span>grid-col-1</span></div>
                        <div class="grid-col-1"><span>grid-col-1</span></div>
                        <div class="grid-col-1"><span>grid-col-1</span></div>
                        <div class="grid-col-1"><span>grid-col-1</span></div>
                        <div class="grid-col-1"><span>grid-col-1</span></div>
                        <div class="grid-col-1"><span>grid-col-1</span></div>
                        <div class="grid-col-1"><span>grid-col-1</span></div>
                        <div class="grid-col-1"><span>grid-col-1</span></div>
                        <div class="grid-col-1"><span>grid-col-1</span></div>
                    </div>
                    <div class="grid-row">
                        <div class="grid-col-2"><span>grid-col-2</span></div>
                        <div class="grid-col-2"><span>grid-col-2</span></div>
                        <div class="grid-col-2"><span>grid-col-2</span></div>
                        <div class="grid-col-2"><span>grid-col-2</span></div>
                        <div class="grid-col-2"><span>grid-col-2</span></div>
                        <div class="grid-col-2"><span>grid-col-2</span></div>
                    </div>
                    <div class="grid-row">
                        <div class="grid-col-4"><span>grid-col-4</span></div>
                        <div class="grid-col-4"><span>grid-col-4</span></div>
                        <div class="grid-col-4"><span>grid-col-4</span></div>
                    </div>
                    <div class="grid-row">
                        <div class="grid-col-2"><span>grid-col-2</span></div>
                        <div class="grid-col-7"><span>grid-col-7</span></div>
                        <div class="grid-col-3"><span>grid-col-3</span></div>
                    </div>
                    <div class="grid-row">
                        <div class="grid-col-4"><span>grid-col-4</span></div>
                        <div class="grid-col-1"><span>grid-col-1</span></div>
                        <div class="grid-col-5"><span>grid-col-5</span></div>
                        <div class="grid-col-2"><span>grid-col-2</span></div>
                    </div>
                    <div class="grid-row">
                        <div class="grid-col-12"><span>grid-col-12</span></div>
                    </div>

                </div>

            </li>
            <li><a href="javascript:void(0)">Tab Bar <i class="fa fa-angle-down fa-indicator"></i></a>


                <ul class="drop-down-tab-bar grid-col-12">
                    <li><a href="#"> <i class="fa fa-tags"></i> Tab Bar <i class="fa fa-angle-right fa-indicator"></i> </a></li>
                    <li><a href="#">Motion Graphics<i class="fa fa-angle-right fa-indicator"></i> </a></li>
                    <li><a href="#">Game Development<i class="fa fa-angle-right fa-indicator"></i> </a></li>
                    <li><a href="#">Business<i class="fa fa-angle-right fa-indicator"></i> </a></li>
                    <li><a href="#">Illustration<i class="fa fa-angle-right fa-indicator"></i> </a></li>
                    <li><a href="#">Web Design<i class="fa fa-angle-right fa-indicator"></i> </a></li>
                    <li><a href="#">Mac Computer Skills<i class="fa fa-angle-right fa-indicator"></i> </a></li>
                </ul>

            </li>
            <li><a href="javascript:void(0)">Images <i class="fa fa-angle-down fa-indicator"></i></a>

                <div class="drop-down grid-col-6 offset-3 offset-4-vertical">
                    <div class="grid-row">
                        <div class="grid-col-3">
                            <h4>Product #1</h4>
                            <a href="#" class="space-0">
                                <img src="images/img.jpg" alt="Image">
                            </a>
                        </div>
                        <div class="grid-col-3">
                            <h4>Product #2</h4>
                            <a href="#" class="space-0">
                                <img src="images/img.jpg" alt="Image">
                            </a>
                        </div>
                        <div class="grid-col-3">
                            <h4>Product #3</h4>
                            <a href="#" class="space-0">
                                <img src="images/img.jpg" alt="Image">
                            </a>
                        </div>
                        <div class="grid-col-3">
                            <h4>Product #4</h4>
                            <a href="#" class="space-0">
                                <img src="images/img.jpg" alt="Image">
                            </a>
                        </div>

                        <div class="grid-row">
                            <div class="grid-col-12">
                                <hr class="space-0">
                                <br>

                                <h3 class="space-0">Here is some content with side images</h3>
                            </div>
                        </div>

                        <div class="grid-row">
                            <div class="grid-col-9">
                                <p class="space-0">
                                    Maecenas eget eros lorem, nec pellentesque lacus quis felis
                                    consequat scelerisque. Aenean dui orci, rhoncus sit amet
                                    tristique eu, tristique sed odio. Praesent ut interdum elit.
                                    Sed in sem mauris. Aenean a commodo mi. Praesent augue lacus.
                                </p>
                                <a href="#">Read more...</a>
                            </div>
                            <div class="grid-col-3">
                                <a href="#" class="space-0">
                                    <img src="images/img.jpg" alt="Image">
                                </a>
                            </div>
                        </div>

                        <div class="grid-row">
                            <div class="grid-col-3">
                                <a href="#" class="space-0">
                                    <img src="images/img.jpg" alt="Image">
                                </a>
                            </div>
                            <div class="grid-col-9">
                                <p class="space-0">
                                    Maecenas eget eros lorem, nec pellentesque lacus quis felis
                                    consequat scelerisque. Aenean dui orci, rhoncus sit amet
                                    tristique eu, tristique sed odio. Praesent ut interdum elit.
                                    Sed in sem mauris. Aenean a commodo mi. Praesent augue lacus.
                                </p>
                                <a href="#">Read more...</a>
                            </div>
                        </div>

                    </div>
                </div>

            </li>
        -->
            <li><a href="<?php echo $tehAbsoluteURL; ?>contactUs.php"><i class="fa fa-envelope-open"></i> Contact</a>
            </li>
        </ul>
        <!-- menu social bar -->
        <!--
        <ul class="menu-social-bar">
            <li data-color="blue"><a href="#"><i class="fa fa-facebook-square"></i></a></li>
            <li data-color="sky-blue"><a href="#"><i class="fa fa-twitter-square"></i></a></li>
            <li data-color="orange"><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
            <li data-color="red"><a href="#"><i class="fa fa-pinterest-square"></i></a></li>
        </ul>
        <!-- menu search bar -->
        <!--
        <ul class="menu-search-bar">
            <li>
                <form method="post" action="#">
                    <label>
                        <input name="menu_search_bar" placeholder="Search" type="search">
                        <i class="fa fa-search"></i>
                    </label>
                </form>
            </li>
        </ul>
    -->
    </section>
</nav>
