<?php
session_cache_expire(10);
session_start();
date_default_timezone_set('America/New_York');

// SET SOME SESSION VARIABLES TO LOG IP ACTIVITY
$_SERVER["REMOTE_ADDR"];

if (empty($_GET['attempts']) )
{
	$_SESSION['attempts'] = 0;
}


function isValidEmail($userID)
{
	if(strlen($userID) > 4 && strlen($userID) < 25)
	{
		//$regexp = "/^[a-zA-Z0-9]/";
		$regexp = "/^[^0-9][A-z0-9_]+([.][A-z0-9_]+)*[@][A-z0-9_]+([.][A-z0-9_]+)*[.][A-z]{2,4}$/";
		if(preg_match($regexp,$userID))
		{
			return true;
		}
	}
	return false;
}

function isPhone($phone)
{
	$regexp = "/^[0-9]/";
	if(preg_match($regexp,$phone))
	{
		if(strlen($phone) != 10)
		{
			return false;
		}
		return true;
	}
	else
	{
		return false;
	}
}

function getRealIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}


?>