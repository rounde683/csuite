<?php
require_once("../sharedFunctions.php");
require_once("../sqlConfig.php");
/* RETURN VALUE */
$arrayToJs = array();
$arrayToJs[0] = array();
$arrayToJs[1] = array();

//INCREMENT THE LOGIN ATTEMPTS
$_SESSION['attempts']++;
$sendMail = true;

// echo $_REQUEST['companyName']."<br />";
// echo $_REQUEST['signedBy']."<br />";
// echo $_REQUEST['jobTitle']."<br />";
// echo $_REQUEST['IAgree']."<br />";
// echo $_REQUEST['elevatorPitch']."<br />";
// echo $_REQUEST['companyURL']."<br />";
// echo $_REQUEST['clientNumber']."<br />";
// echo $_REQUEST['projectedGrowth']."<br />";
// echo $_REQUEST['topCompetitors']."<br />";
// echo $_REQUEST['legalEntityYesNo']."<br />";
// echo $_REQUEST['formationDocs']."<br />";
// echo $_REQUEST['lookingForFinancing']."<br />";
// echo "<br /><br /><br />";

$id = md5(uniqid(rand(), true));
$companyName = $_REQUEST['companyName'];
$signedBy = $_REQUEST['signedBy'];
$jobTitle = $_REQUEST['jobTitle'];
$IAgree = $_REQUEST['IAgree'];
$elevatorPitch = $_REQUEST['elevatorPitch'];
$companyURL = $_REQUEST['companyURL'];
$clientNumber = $_REQUEST['clientNumber'];
$projectedGrowth = $_REQUEST['projectedGrowth'];
$topCompetitors = $_REQUEST['topCompetitors'];
$legalEntityYesNo = $_REQUEST['legalEntityYesNo'];
$formationDocsYesNo = $_REQUEST['formationDocs'];
$lookingForFinancingYesNo = $_REQUEST['lookingForFinancing'];

//MARKETING
$marketingMetricsYesNo = $_REQUEST['marketingMetricsYesNo'];
$marketingStrategies = serialize($_REQUEST['marketingStrategies']);
$marketingStrategy = $_REQUEST['marketingStrategy'];

//LELGAL
$legalAttorney = $_REQUEST['usingAnAtorney'];
$legalForms = $_REQUEST['formsChecklists'];
$legalVendors = $_REQUEST['engageVendors'];
$legalConcerns = $_REQUEST['legalConcerns'];


//SECURITY
$securityFrameworks = serialize($_REQUEST['securityFrameworks']);
$securityProgramYesNo = $_REQUEST['securityProgram'];
$securityProgramMaturity = $_REQUEST['securityProgramMaturity'];
$securityThirdParty = $_REQUEST['securityThirdParty'];
$securityProgramConcerns = $_REQUEST['securityProgramConcerns'];


// prepare sql and bind parameters
$stmt = $conn->prepare("INSERT INTO questionnaire (id,company_name,signed_by,job_title,IAgree,elevator_pitch,company_url,
 client_number, projected_growth,top_competitors,legal_entity_yesno,formation_docs_yesno,financing_yesno,
 marketing_metrics_yesno,marketing_strategies,marketing_strategy,
 security_frameworks,security_program,security_maturity,security_third_party,security_concerns,
 legal_usinganattorney,legal_forms,legal_vendors,legal_concerns)
VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
$stmt->execute(array($id,$companyName,$signedBy,$jobTitle,$IAgree,$elevatorPitch,$companyURL,$clientNumber,$projectedGrowth,
	$topCompetitors,$legalEntityYesNo,$formationDocsYesNo,$lookingForFinancingYesNo,
	$marketingMetricsYesNo,$marketingStrategies,$marketingStrategy,
	$securityFrameworks,$securityProgramYesNo,$securityProgramMaturity,$securityThirdParty,$securityProgramConcerns,
	$legalAttorney,$legalForms,$legalVendors,$legalConcerns));


// 	$arrayToJs[0][0] = 'contactNameField';
// 	$arrayToJs[0][1] = false;
// 	$arrayToJs[0][2] = "Form is Invalid! Too many submissions.";
// 	$sendMail = false;

// if($_SESSION['attempts'] > 600)
// {
	// $arrayToJs[0][0] = 'contactNameField';
	// $arrayToJs[0][1] = false;
	// $arrayToJs[0][2] = "Form is Invalid! Too many submissions.";
	// $sendMail = false;
// }
// if (empty($_GET['contactNameField']) )
// {
	// $arrayToJs[0][0] = 'contactNameField';
	// $arrayToJs[0][1] = false;
	// $arrayToJs[0][2] = "Form is Invalid!";
	// $sendMail = false;
// }

// if (empty($_GET['contactEmailField']) )
// {
	// $arrayToJs[0][0] = 'contactNameField';
	// $arrayToJs[0][1] = false;
	// $arrayToJs[0][2] = "Form is Invalid!";
	// $sendMail = false;
// }
// if (empty($_GET['contactCompanyNameField']) )
// {
	// $arrayToJs[0][0] = 'contactCompanyNameField';
	// $arrayToJs[0][1] = false;
	// $arrayToJs[0][2] = "Form is Invalid!";
	// $sendMail = false;
// }

// if (empty($_GET['contactPhoneField']) )
// {
	// $arrayToJs[0][0] = 'contactPhoneField';
	// $arrayToJs[0][1] = false;
	// $arrayToJs[0][2] = "Form is Invalid!";
	// $sendMail = false;
// }

// if (empty($_GET['contactMessageField']) )
// {
	// $arrayToJs[0][0] = 'contactNameField';
	// $arrayToJs[0][1] = false;
	// $arrayToJs[0][2] = "Form is Invalid!";
	// $sendMail = false;
// }

// if($sendMail == true)
// {
	// $qName = $_GET['contactNameField'];
	// $qEmail = $_GET['contactEmailField'];
	// $qCompanyName = $_GET['contactCompanyNameField'];
	// $qPhone = $_GET['contactPhoneField'];
	// $qMessage = $_GET['contactMessageField'];
	
	// $htmlEmailMessage = '
		// <div>Name: '.$qName.'</div>
		// <div>Email: '.$qEmail.'</div>
		// <div>Name: '.$qCompanyName.'</div>
		// <div>Phone: '.$qPhone.'</div>
		// <div>Message:</div>
		// <br />
		// <div>'.$qMessage.'</div>
	// ';
	
	// $headers  = 'MIME-Version: 1.0' . "\r\n";
	// $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	// $headers .= 'To: <robjunderwood@gmail.com>' . "\r\n";
	// $headers .= 'From: Contact Us Form<info@fractional-csuite.com>' . "\r\n";
    
	// mail("robjunderwood@gmail.com", 'Website Inquiry', $htmlEmailMessage,$headers);

//  	$headers  = 'MIME-Version: 1.0' . "\r\n";
//  	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
//  	$headers .= 'To: <'.$qEmail.'>' . "\r\n";
//  	$headers .= 'From: EZ Leads Quote <gommer683@gmail.com>' . "\r\n";
// 	
// 	
// 	$EZAutoResponderSubject = "Your FREE confidential quote is on the way!";
// 	$fNameSalutationArray = explode(" ", $qName);
// 	$fNameSalutation = $fNameSalutationArray[0];
// 	$EZAutoResponderMessage = file_get_contents("PHPclasses/phpMailer/EZautoResponderEmail.html");
// 	$EZAutoResponderMessage = str_replace("[FNAME]",$fNameSalutation,$EZAutoResponderMessage);

// 	mail($qEmail,$EZAutoResponderSubject,$EZAutoResponderMessage,$headers);
//}


///////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// END FORM VALIDATION ///////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

//echo json_encode($arrayToJs);

?>